            function exibir(id){
                var destaque = document.getElementById('destaque');
                var img = document.createElement('img');
                var src =  document.getElementById(id);
                img.setAttribute('src', src.getAttribute('src'));
                img.setAttribute('width', '400px');
                img.setAttribute('height', '250px');
                destaque.setAttribute("width", "400px");
                destaque.innerHTML = "";
                destaque.appendChild(img);
                all_opaque(id);
                select = id;
            }

            function onover(id){
                var img = document.getElementById(id);
                img.style.opacity = '1';
                img.style.filter = 'alpha(opacity=100)';
            }

            function outover(id){
                 var img = document.getElementById(id);
                 if(select!=id){
                     img.style.opacity = '0.5';
                     img.style.filter = 'alpha(opacity=50)';
                 }

            }

            function all_opaque(ex){                
                for(id = 1; id <= total; id++){
                    var img = document.getElementById(id);
                    if(id!=ex){
                        img.style.opacity = '0.5';
                        img.style.filter = 'alpha(opacity=50)';
                    }else{
                         img.style.opacity = '1';
                         img.style.filter = 'alpha(opacity=100)';
                    }
                }
            }

