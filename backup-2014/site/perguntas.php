<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
 <style type="text/css">

 img, div { behavior: url(iepngfix.htc) }
 

 </style>

<title>Laborat&oacute;rios Gaspar</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<div id="pai">
<div id="principal">
<div id="topo"><? include 'topo.php' ?></div>
<div id="animacao"><? include 'logo.php' ?></div>
<div id="lateral_esquerda"><? include 'lateral_esquerda.php' ?></div>
<div id="conteudo_index">
  <table width="476" border="0" align="center" cellpadding="0" cellspacing="0" >
    <tr>
      <td height="17" align="center" valign="top" style="background:url(imagens/bg_topo_noticialista.png) bottom center no-repeat">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" valign="top" style="background:url(imagens/bg_meio_noticialista.png) top center repeat-y"><table width="448" border="0" align="center" cellpadding="0" cellspacing="0">
        
        <tr>
          <td height="42" align="left" valign="top"><img src="imagens/perguntas_topo.png" width="251" height="36" /></td>
        </tr>
        <tr>
          <td align="left" valign="top" class="titulo_noticia_princ" ><span class="titulo_noticia_dentro">Novo biomarcador para eventos 
            cardiovasculares</span><br />
            <br />
            <span class="texto_noticia_dentro">Cerca de 40% das v&iacute;timas de tr&aacute;fico de pessoas em Portugal s&atilde;o mulheres de nacionalidade brasileira. Este &eacute; o resultado do Relat&oacute;rio Anual de 2009 do Observat&oacute;rio do Tr&aacute;fico de Seres Humanos, &oacute;rg&atilde;o ligado ao Minist&eacute;rio da Administra&ccedil;&atilde;o Interna (Interior) portugu&ecirc;s. </span>
            <p class="texto_noticia_dentro">&quot;Podemos tra&ccedil;ar um perfil da v&iacute;tima. &Eacute; mulher, brasileira e o tr&aacute;fico destina-se &agrave; explora&ccedil;&atilde;o sexual. &Eacute; solteira, com mais de 25 anos, e vem para Portugal com uma proposta de trabalho&quot;, afirma Joana Daniel Wrabetz, respons&aacute;vel pelo estudo. Ela conta que a maior parte das brasileiras v&iacute;timas de tr&aacute;fico v&ecirc;m de Goi&aacute;s, Minas Gerais e de Estados do Nordeste.</p>
            <p class="texto_noticia_dentro">O estudo foi feito baseado em 84 casos sinalizados durante o ano de 2009, dos quais sete j&aacute; foram levados a julgamento. Em rela&ccedil;&atilde;o aos agressores, tamb&eacute;m foi estabelecido um perfil.</p>
            <p class="texto_noticia_dentro">&quot;Geralmente &eacute; um portugu&ecirc;s que conhece os prost&iacute;bulos onde pode colocar as v&iacute;timas, muitas vezes em parceria com um estrangeiro&quot;, relata Joana.</p>
            <p class="texto_noticia_dentro">Ela distingue o tr&aacute;fico da imigra&ccedil;&atilde;o ilegal para a prostitui&ccedil;&atilde;o. &quot;No tr&aacute;fico, depois de entrar no pa&iacute;s, as v&iacute;timas perdem seus direitos, est&atilde;o a ser violentadas e ficam reduzidas a uma situa&ccedil;&atilde;o de escravatura. O fato de que muitas brasileiras tenham vindo sabendo que iam trabalhar na prostitui&ccedil;&atilde;o n&atilde;o pode servir de desculpa para justificar o tr&aacute;fico.&quot;</p>
            <p class="texto_noticia_dentro">Muitas vezes, al&eacute;m de situa&ccedil;&otilde;es de c&aacute;rcere privado, as v&iacute;timas de tr&aacute;fico ficam sem documentos. Normalmente, para impedir que as v&iacute;timas de tr&aacute;fico fujam, os documentos da v&iacute;tima s&atilde;o retirados.</p>
            <p class="texto_noticia_dentro">&quot;O tr&aacute;fico de seres humanos p&otilde;e em causa a dignidade dos seres humanos. Por isso, o c&oacute;digo penal estabeleceu como crime grave a oculta&ccedil;&atilde;o de documentos ou sua destrui&ccedil;&atilde;o&quot;, afirmou o ministro da Administra&ccedil;&atilde;o Interna, Rui Pereira.</p>
            <p class="texto_noticia_dentro">N&atilde;o h&aacute; dados em Portugal sobre o total de v&iacute;timas. &quot;Este &eacute; o segundo ano que fazemos o relat&oacute;rio. N&atilde;o tenho meios para dar uma estimativa do universo total de v&iacute;timas de tr&aacute;fico. Ainda n&atilde;o foi poss&iacute;vel reunir dados hist&oacute;ricos para elaborar modelos para predizer a realidade&quot;, relata Paulo Jo&atilde;o, da Dire&ccedil;&atilde;o Geral da Administra&ccedil;&atilde;o Interna, &oacute;rg&atilde;o ligado ao Minist&eacute;rio da Administra&ccedil;&atilde;o Interna.</p>
            <p class="texto_noticia_dentro">Maior comunidade</p>
            <p class="texto_noticia_dentro">Para o diretor do Servi&ccedil;o de Estrangeiros e Fronteiras, Manuel Jarmela Paulus, o maior n&uacute;mero de brasileiros entre as v&iacute;timas est&aacute; relacionado apenas &agrave; dimens&atilde;o da comunidade - com 100 mil pessoas, mais de 20% do total de imigrantes no pa&iacute;s.</p>
            <p class="texto_noticia_dentro">&quot;Isso n&atilde;o tem nada a ver com nenhuma particularidade do pa&iacute;s. Apenas &eacute; a comunidade mais numerosa em Portugal&quot;, afirma.</p>
            <p class="texto_noticia_dentro">O segundo grupo mais numeroso de v&iacute;timas &eacute; proveniente de pa&iacute;ses do Leste Europeu.</p>
            <p class="texto_noticia_dentro">Segundo Paulus, para combater o tr&aacute;fico de pessoas, o Servi&ccedil;o de Estrangeiros est&aacute; trabalhando com as autoridades brasileiras. &quot;As parcerias com a Pol&iacute;cia Federal &ecirc;m sido exemplares. No Brasil, a quest&atilde;o do tr&aacute;fico de pessoas tamb&eacute;m preocupa as autoridades brasileiras.&quot;</p>
            <p><span class="texto_noticia_dentro">Sem dar n&uacute;meros de opera&ccedil;&otilde;es e de pessoas que teriam sido detidas por tr&aacute;fico, ele indica como resultados da parceria com a Pol&iacute;cia Federal a presen&ccedil;a de agentes brasileiros em Portugal, tomando parte de opera&ccedil;&otilde;es do SEF e de portugueses no Brasil, em opera&ccedil;&otilde;es realizadas pela Pol&iacute;cia Federal.</span><br />
            </p></td>
        </tr>
        <tr>
          <td height="30" align="left" valign="middle"><img src="imagens/leia_tbm.png" width="99" height="18" /></td>
        </tr>
        <tr>
          <td height="40" align="left" valign="middle" style=" background:url(imagens/bg_noticia_lista_index.png) top center no-repeat"><table width="440" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td width="14" align="center" valign="middle"><img src="imagens/seta_noticias.png" width="6" height="26" /></td>
                <td width="426" height="30" align="left" valign="middle" class="texto_noticia"><a href="#">Novo biomarcador para eventos cardiovasculares biomarcador para eventos
                  cardiovasculares</a></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td height="7" align="left" valign="middle"><img src="imagens/transparent.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="40" align="left" valign="middle" style=" background:url(imagens/bg_noticia_lista_index.png) top center no-repeat"><table width="440" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td width="14" align="center" valign="middle"><img src="imagens/seta_noticias.png" width="6" height="26" /></td>
                <td width="426" height="30" align="left" valign="middle" class="texto_noticia"><a href="#">Novo biomarcador para eventos cardiovasculares biomarcador para eventos
                  cardiovasculares</a></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td height="7" align="left" valign="middle"><img src="imagens/transparent.gif" alt="" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="40" align="left" valign="middle" style=" background:url(imagens/bg_noticia_lista_index.png) top center no-repeat"><table width="440" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td width="14" align="center" valign="middle"><img src="imagens/seta_noticias.png" width="6" height="26" /></td>
                <td width="426" height="30" align="left" valign="middle" class="texto_noticia"><a href="#">Novo biomarcador para eventos cardiovasculares biomarcador para eventos
                  cardiovasculares</a></td>
              </tr>
          </table></td>
        </tr>

        <tr>
          <td height="7" align="right"><img src="imagens/transparent.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="30" align="right"><img src="imagens/btn_vejamais.png" width="79" height="26" /></td>
        </tr>
      </table>        
        </td>
    </tr>
    <tr>
      <td height="19" align="center" valign="top" style="background:url(imagens/bg_baixo_noticialista.png) top center no-repeat">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="lateral_direita">
  <? include 'lateral_direita.php' ?>
</div>
</div><!--fecha div principal-->
<div id="rodape"><? include 'rodape.php' ?></div>

</div><!--fecha div pai-->

  
   
</body>
</html>
