<?
	require 'classes/enquetes.php';
	$enquetes->enquete_votos();
	$pcnt = $enquetes->pcnt;
	$res = $enquetes->res;
	$opt = $enquetes->opt;
	$pergunta = $enquetes->pergunta;	
	$total = $enquetes->tot;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratorio Gaspar - Resultado de enquete</title>
</head>
<style>
	body {
		margin-left: 4px;
		margin-top: 4px;
		margin-right: 4px;
		margin-bottom: 4px;
                		background-color:#FFFFFF;
                background-image:url(http://www.laboratoriogaspar.com.br/site/imagens/bg_informe2.png);
                background-position: left;
                background-position: top;
                background-repeat: no-repeat;
	}
	#corpo {
		width: 460px;
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:11px;

	}
	#topo {
		background-repeat:no-repeat;
		background-color:#FFFFFF;
		height: 126px;
	}
	#one {
		background-color:#FFFFFF;
		padding: 5px 5px 0px 0px;
		height: 40px;
	}
	#two {
		background-color:#FFFFFF;
		padding: 5px 5px 0px 0px;
		height: 40px;
	}	
	#btns {
		height: 20px;
		text-align:center;
		padding: 5px 5px 0px 0px;
	}
	b {
		text-shadow:#999999;
		font-size: 10px;
	}
	a, a:visited {
		color:#000000;
	}
	a:hover {
		color:#666666;
	}
	#graph {
		background-color:#060;
		border: 2px solid #060;
		height:14px;
}
</style>
<body>
	<div id="corpo">
<table width="460">
  <!--DWLayoutTable-->
  <tr>
    <th height="40" colspan="3" valign="top" scope="col"><? echo $pergunta; ?></th>
  </tr>
  <tr bgcolor="#E6FFE6">
    <th width="173" align="left" scope="row"><? echo $opt[0]; ?></th>
    <td width="227"><div id="graph" style="width:<? echo $pcnt[0] * 2; ?>px"></div></td>
    <td width="44"><? echo $pcnt[0]; ?>%</td>
  </tr>
  <tr bgcolor="#E6FFE6">
    <th align="left" scope="row"><? echo $opt[1]; ?></th>
    <td><div id="graph" style="width:<? echo $pcnt[1] * 2; ?>px"></div></td>
    <td><? echo $pcnt[1]; ?>%</td>
  </tr>
  <tr bgcolor="#E6FFE6">
    <th align="left" scope="row"><? echo $opt[2]; ?></th>
    <td><div id="graph" style="width:<? echo $pcnt[2] * 2; ?>px"></div></td>
    <td><? echo $pcnt[2]; ?>%</td>
  </tr>
  <tr>
    <th height="21" colspan="3" valign="top" scope="col"></th>
<!--    <th height="21" colspan="3" valign="top" scope="col">Total de votos: <?// echo $total; ?></th>-->
  </tr>
</table>
	</div>
</body>
</html>
