<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
 <style type="text/css">

 img, div { behavior: url(iepngfix.htc) }
 

 </style>

<title>Laborat&oacute;rios Gaspar</title>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<div id="pai">
<div id="principal">
<div id="topo"><? include 'topo.php' ?></div>
<div id="animacao"><? include 'logo.php' ?></div>
<div id="lateral_esquerda"><? include 'lateral_esquerda.php' ?></div>
<div id="conteudo_index">
  <table width="476" border="0" align="center" cellpadding="0" cellspacing="0" >
    <tr>
      <td height="17" align="center" valign="top" style="background:url(imagens/bg_topo_noticialista.png) bottom center no-repeat">&nbsp;</td>
    </tr>
    <tr>
      <td height="665" align="center" valign="top" style="background:url(imagens/bg_meio_noticialista.png) top center repeat-y"><table width="448" border="0" align="center" cellpadding="0" cellspacing="0">
        
        <tr>
          <td height="42" align="left" valign="top"><img src="imagens/parceiros_topo.png" width="248" height="39" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><p class="texto_noticia_dentro">O Laborat&oacute;rio Gaspar &eacute; uma empresa privada, fundada em Dezembro de 1975, com sede em S&atilde;o Lu&iacute;s - MA. Sua matriz, que inicialmente funcionou na rua de Santaninha, 529, hoje ocupa 1.300 m&sup2; na Rua dos Afogados, 757, Centro, desde 1981, e conta com 13 unidades de atendimento espalhadas em diversos bairros da cidade. Tendo como base de sustenta&ccedil;&atilde;o do seu crescimento a utiliza&ccedil;&atilde;o &uacute;nica e exclusiva de capital pr&oacute;prio.</p>
            <p class="texto_noticia_dentro">Iniciou suas atividades atuando basicamente em An&aacute;lises Cl&iacute;nicas, alicer&ccedil;ado em uma proposta clara de fornecer subs&iacute;dios para diagn&oacute;sticos e acompanhamento de diversas patologias utilizando o que havia de mais moderno em tecnologia. Foi esse o grande motivo para seu atual sucesso.</p>
            <p class="texto_noticia_dentro">Com o passar do tempo a busca pela efici&ecirc;ncia e pela modernidade tecnol&oacute;gica foi cada vez mais intensa produzindo uma empresa moderna e totalmente informatizada, est&iacute;mulo para novos avan&ccedil;os. S&atilde;o muitos os equipamentos constantemente adquiridos que impressionam qualquer visitante, da &aacute;rea de an&aacute;lises cl&iacute;nicas ou fora dela.</p>
            <p class="texto_noticia_dentro">Tendo o cliente como sua principal riqueza, grandes inova&ccedil;&otilde;es est&atilde;o para acontecer. Assim que, al&eacute;m dos resultados ficarem dispon&iacute;veis na internet, ser&atilde;o criados v&aacute;rios postos eletr&ocirc;nicos, estrategicamente localizados, onde os clientes imprimir&atilde;o seus pr&oacute;prios laudos de resultados.</p>
            <p class="texto_noticia_dentro">Os investimentos em equipamentos, instala&ccedil;&otilde;es f&iacute;sicas e sobretudo em qualifica&ccedil;&atilde;o de pessoal resultaram em um grande programa de qualidade, precedido de controles internos e externos que em breve levar&atilde;o o Laborat&oacute;rio Gaspar a conquistar o certificado de acredita&ccedil;&atilde;o, pioneiro no Maranh&atilde;o.</p></td>
        </tr>
        
      </table>        
        </td>
    </tr>
    <tr>
      <td height="19" align="center" valign="top" style="background:url(imagens/bg_baixo_noticialista.png) top center no-repeat">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="lateral_direita">
  <? include 'lateral_direita.php' ?>
</div>
</div><!--fecha div principal-->
<div id="rodape"><? include 'rodape.php' ?></div>

</div><!--fecha div pai-->

  
   
</body>
</html>
