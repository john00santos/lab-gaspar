<?
	require_once 'classes/faq.php';

	
	function tratarImgs($id) {
		require_once '../classes/redutor.php';
		$pastaImgRoot = $_SERVER['DOCUMENT_ROOT']."/site/imagens/noticias/".$id."/";
		@mkdir($pastaImgRoot);
		$tempfile = $pastaImgRoot."temp_pequena.jpg";
		if (move_uploaded_file($_FILES['imagempequena']['tmp_name'], $tempfile)) {
			$origem = $tempfile;
			$destino = str_replace("temp_pequena.jpg", "foto_miniatura.jpg", $tempfile);
			$dest_width = 166;
			$dest_height = 221;
			$max_width = 1000;
			$max_height = 221;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);	
			unlink($tempfile);
		}

		$tempfile = $pastaImgRoot."temp_grande.jpg";	
		if (move_uploaded_file($_FILES['imagemgrande']['tmp_name'], $tempfile)) {				
			$origem = $tempfile;
			$destino = str_replace("temp_grande.jpg", "foto_grande.jpg", $tempfile);
			$dest_width = 442;
			$dest_height = 157;
			$max_width = 1000;
			$max_height = 157;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);
			unlink($tempfile);
		}	
	}
	
	
	function trataDados() {
		
		require_once '../classes/textActions.php';
		require_once '../classes/dater.php';
		require_once '../classes/permalinks.php';
		
		$fields['id'] = $id = $_GET['id'];
		$fields['titulo'] = $titulo = utf8_decode($_POST['fields']['titulo']);
		$fields['permalink'] = $permalink = $permalinks->generate($_POST['fields']['titulo']);        
		$bitlyurl = "http://laboratoriogaspar.com.br/starky/objects/bitly/bitly_generator.php?url=".urlencode("http://laboratoriogaspar.com.br/site/faq/".$id."/".$permalink);
		$fields['bitly'] = $bitly = file_get_contents($bitlyurl);
		$fields['texto_twitter'] = "$titulo";
		$fields['texto'] = $texto = utf8_decode(str_replace("'", "\'", $textActions->strTrimTotal($_POST['ftexts']['1'])));
		
		
		
		return $fields;
	}
	
		
	switch($_GET['act']) {
		case "criar":			
			
			$fields = trataDados();		
			
			
			$faq->addFaq($fields['id'], $fields['titulo'], $fields['permalink'], $fields['texto_twitter'], $fields['texto'], $fields['bitly']);
			
			
			
			?>
            	<script>
					alert("Faq adicionada com sucesso.");
					document.location.href="listar.php";					
				</script>
            <?
			break;
		case "editar":
					
			$fields = trataDados();	
			
			$faq->editFaq($fields['id'], $fields['titulo'], $fields['permalink'], $fields['texto_twitter'],  $fields['texto'], $fields['bitly']);
			tratarImgs($id);
	
			?>
            	<script>
					alert("Faq editada com sucesso.");
					document.location.href="listar.php";				
				</script>
            <?
			break;
		case "deletar":
			$faq->deleteFaq($_GET['id']);
			?>
            	<script>
					alert("Faq deletada com sucesso");
					document.location.href="listar.php";
				</script>
            <?
			break;

		case "activeFaq":
			$faq->activeFaq($_GET['id']);
			
			?>
            	<script>
					alert("Faq ativada com sucesso");
					document.location.href="listar.php";
				</script>
            <?
			break;
	}
	//header("Location: http://".$_GET['host']."/bandaxavecodemenina.com.br/starky/noticias/listar.php?ipp=".$_GET['ipp']."&pg=".$_GET['pg']);
?>