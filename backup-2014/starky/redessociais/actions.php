<?
	require_once 'classes/redessociais.php';

	
	function tratarImgs($id) {
		require_once '../classes/redutor.php';
		$pastaImgRoot = $_SERVER['DOCUMENT_ROOT']."/site/imagens/noticias/".$id."/";
		@mkdir($pastaImgRoot);
		$tempfile = $pastaImgRoot."temp_pequena.jpg";
		if (move_uploaded_file($_FILES['imagempequena']['tmp_name'], $tempfile)) {
			$origem = $tempfile;
			$destino = str_replace("temp_pequena.jpg", "foto_miniatura.jpg", $tempfile);
			$dest_width = 166;
			$dest_height = 221;
			$max_width = 1000;
			$max_height = 221;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);	
			unlink($tempfile);
		}

		$tempfile = $pastaImgRoot."temp_grande.jpg";	
		if (move_uploaded_file($_FILES['imagemgrande']['tmp_name'], $tempfile)) {				
			$origem = $tempfile;
			$destino = str_replace("temp_grande.jpg", "foto_grande.jpg", $tempfile);
			$dest_width = 442;
			$dest_height = 157;
			$max_width = 1000;
			$max_height = 157;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);
			unlink($tempfile);
		}	
	}
	
	
	function trataDados() {
		
		
		$fields['orkut'] = $orkut = utf8_decode($_POST['fields']['orkut']);
		$fields['twitter'] = $twitter = utf8_decode($_POST['fields']['twitter']);
		$fields['facebook'] = $facebook = utf8_decode($_POST['fields']['facebook']);
		$fields['myspace'] = $myspace = utf8_decode($_POST['fields']['myspace']);
		
		
		return $fields;
	}
	
		
	switch($_GET['act']) {
		case "editar":			
			
			$fields = trataDados();		
			
			
			$redessociais->alterar($fields['orkut'], $fields['twitter'], $fields['facebook'], $fields['myspace']);
			
			
			
			?>
            	<script>
					alert("Dados alterados com sucesso.");
					document.location.href="listar.php";					
				</script>
            <?
			break;
	}
	//header("Location: http://".$_GET['host']."/bandaxavecodemenina.com.br/starky/noticias/listar.php?ipp=".$_GET['ipp']."&pg=".$_GET['pg']);
?>