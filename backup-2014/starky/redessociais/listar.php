<?
	require '../scriptsforload.php';
	require 'classes/redessociais.php';	
	$redessociais->loadValues();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>



<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador #pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#redessociais {
		float:none;
		clear:both;
	}
</style>
<script src="../objects/textcounter/js/textcounter.js" type="text/javascript" language="javascript"></script>
<script src="../objects/bitly/js/bitly.js" type="text/javascript" language="javascript"></script>
<script src="../filesmanager/js/basic_functions.js" type="text/javascript" language="javascript"></script>
<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	
<? require 'menu.php'; ?>

<div id="corpo">
<div class="titlearea">Gerenciar Redes Sociais</div>
<form name="form" method="post" action="actions.php?act=editar&host=<? echo $prop['host']; ?>" enctype="multipart/form-data"/>

<input type="hidden" name="MAX_FILE_SIZE" value="21474836480">




<h4>Orkut:</h4>
<input type="text" name="fields[orkut]" maxlength="70" class="text_titulo" value="<? echo $redessociais->fields['orkut']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<small>Digite endereço completo do perfil:<br />
Ex: <strong>http://www.orkut.com/Home#Profile?id=100000199474242</strong></small>
<br /><br />

<h4>Twitter:</h4>
<input type="text" name="fields[twitter]" maxlength="70" class="text_titulo" value="<? echo $redessociais->fields['twitter']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<Small>Digite somente o nome de usuário:<br />
Ex: http://twitter.com/<strong>usuario</strong>
</Small>
<br /><br />

<h4>Facebook:</h4>
<input type="text" name="fields[facebook]" maxlength="70" class="text_titulo" value="<? echo $redessociais->fields['facebook']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<small>Digite endereço completo do perfil:<br />
Ex: <strong>http://www.facebook.com/profile.php?id=100000199474242</strong></small>
<br /><br />

<h4>Myspace:</h4>
<input type="text" name="fields[myspace]" maxlength="70" class="text_titulo" value="<? echo $redessociais->fields['myspace']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<small>Digite endereço completo do perfil:<br />
Ex: <strong>http://www.myspace.com/glauromano</strong></small>
<br /><br />


<input type="submit" value="Enviar" onclick="saveall()" />

<div id="control"></div>						
</form>

</div>
</body>
</html>