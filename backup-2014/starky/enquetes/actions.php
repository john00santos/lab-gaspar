<?
	require_once 'classes/enquetes.php';

	
	function tratarImgs($id) {
		require_once '../classes/redutor.php';
		$pastaImgRoot = $_SERVER['DOCUMENT_ROOT']."/site/imagens/noticias/".$id."/";
		@mkdir($pastaImgRoot);
		$tempfile = $pastaImgRoot."temp_pequena.jpg";
		if (move_uploaded_file($_FILES['imagempequena']['tmp_name'], $tempfile)) {
			$origem = $tempfile;
			$destino = str_replace("temp_pequena.jpg", "foto_miniatura.jpg", $tempfile);
			$dest_width = 166;
			$dest_height = 221;
			$max_width = 1000;
			$max_height = 221;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);	
			unlink($tempfile);
		}

		$tempfile = $pastaImgRoot."temp_grande.jpg";	
		if (move_uploaded_file($_FILES['imagemgrande']['tmp_name'], $tempfile)) {				
			$origem = $tempfile;
			$destino = str_replace("temp_grande.jpg", "foto_grande.jpg", $tempfile);
			$dest_width = 442;
			$dest_height = 157;
			$max_width = 1000;
			$max_height = 157;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);
			unlink($tempfile);
		}	
	}
	
	
	function trataDados() {
		
		require_once '../classes/textActions.php';
		require_once '../classes/dater.php';
		require_once '../classes/permalinks.php';
		
		$fields['id'] = $id = $_GET['id'];
		$fields['titulo'] = $titulo = utf8_decode($_POST['fields']['titulo']);
		$fields['opt1'] = $opt1 = utf8_decode($_POST['fields']['opt1']);
		$fields['opt2'] = $opt2 = utf8_decode($_POST['fields']['opt2']);
		$fields['opt3'] = $opt3 = utf8_decode($_POST['fields']['opt3']);
		$fields['res1'] = $res1 = utf8_decode($_POST['fields']['res1']);
		$fields['res2'] = $res2 = utf8_decode($_POST['fields']['res2']);
		$fields['res3'] = $res3 = utf8_decode($_POST['fields']['res3']);

		
		return $fields;
	}
	
		
	switch($_GET['act']) {
		case "criar":			
			
			$fields = trataDados();		
			
			
			$enquetes->addEnquete($fields['id'], $fields['titulo'], $fields['opt1'], $fields['opt2'], $fields['opt3']);
			
			
			
			?>
            	<script>
					alert("Enquete adicionada com sucesso.");
					document.location.href="listar.php";					
				</script>
            <?
			break;
		case "editar":
					
			$fields = trataDados();	
			
			$enquetes->editEnquete($fields['id'], $fields['titulo'], $fields['opt1'], $fields['opt2'], $fields['opt3'],$fields['res1'], $fields['res2'], $fields['res3']);
			tratarImgs($id);
	
			?>
            	<script>
					alert("Enquete editada com sucesso.");
					document.location.href="listar.php";				
				</script>
            <?
			break;
		case "deletar":
			$enquetes->deleteEnquete($_GET['id']);
			?>
            	<script>
					alert("Enquete deletada com sucesso");
					document.location.href="listar.php";
				</script>
            <?
			break;

		case "activeEnquete":
			$enquetes->activeEnquete($_GET['id']);
			
			?>
            	<script>
					alert("Enquete ativada com sucesso");
					document.location.href="listar.php";
				</script>
            <?
			break;
	}
	//header("Location: http://".$_GET['host']."/bandaxavecodemenina.com.br/starky/noticias/listar.php?ipp=".$_GET['ipp']."&pg=".$_GET['pg']);
?>