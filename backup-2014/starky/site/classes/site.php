<?
	require '../classes/permalinks.php';
	class adm_site extends permalinks {
		var $categorias;
		static private $noticiasConn;
		static private $broadcastConn;
		
		
		static private $sys;	
		private $info;
			
		public function __construct() {
			
			$this->sys['db'] = "gasparhp";
			$this->sys['prefix'] = "noticias_";
			$this->sys['prefix2'] = "enquetes_";
			$this->sys['prefix3'] = "faq_";
			$this->sys['prefix4'] = "informe_";
			$this->sys['prefix5'] = "redessociais_";
			$this->sys['prefix6'] = "novidades_";
			$this->sys['prefix7'] = "banners_";
			
			
			if ($_SERVER['REMOTE_ADDR'] != "127.0.0.1") {
				$host = "sqlsrv.elo.com.br";
				$user = "gasparhp";
				$pass = "1oPYGsaL";
			}
			else {
				$host = "localhost";
				$user = "root";
				$pass = "";
			}
			
			$this->info['conn'] = mysql_connect($host, $user, $pass) or die ("Erro ao conectar");
			mysql_select_db($this->sys['db'], $this->info['conn']) ;
			
		}
		
		
		private function mquery($sql) {
			$query = mysql_query($sql, $this->info['conn']) or die (mysql_error($query) . ": $sql");
			return ($query);
		}
		
		function redir($msg, $url = "index.php") {
			?>
            	<script language="javascript">
					alert("<? echo $msg; ?>");
					document.location.href = "<? echo $url; ?>";
				</script>
            <?
		}
		
		
		function write($destino, $conteudo) {
			$file = fopen($destino, "w+");
			fwrite($file, $conteudo);
			return true;
		}
		
		
		function generateBannersSlideshow($alert = true) {
			$template_addr = "../../site/banner_object_template.xml";
			$template2_addr = "../../site/banner_template.xml";
			
			$detfile = "../../site/banner.xml";
			
			$template = file_get_contents($template_addr);
			$template2 = file_get_contents($template2_addr);
			
			$sql = "SELECT ".$this->sys['prefix7']."info.id, ".$this->sys['prefix7']."info.titulo, ".$this->sys['prefix7']."info.link FROM `".$this->sys['prefix7']."info` ORDER BY id DESC LIMIT 0, 5";

			$query = $this->mquery($sql);
			
			while ($result = mysql_fetch_object($query)) {
				$id = $result->id;
				$link = $result->link;
				$titulo = $result->titulo;
				
				$imagem = "imagens/banners/$id/banner.jpg";
				
				$titulo = utf8_encode($result->titulo);
				
				$temp = str_replace("[IMAGEM-NOTICIA]", $imagem, $template);				
				$temp = str_replace("[LINK-NOTICIA]", $link, $temp);
				$temp = str_replace("[DESCRICAO-NOTICIA]", $titulo, $temp);
				
				$conteudo .= $temp;  
			}

			$conteudo2 = str_replace("[REPLACE-TEMPLATE]", $conteudo, $template2);
			
			$this->write($detfile, $conteudo2);		
		}

		
		function generateIndexNoticiasSlideshow($alert = true) {
			$template_addr = "../../site/slideshow_object_template.xml";
			$template2_addr = "../../site/slideshow_template.xml";
			
			$detfile = "../../site/slideshow.xml";
			
			$template = file_get_contents($template_addr);
			$template2 = file_get_contents($template2_addr);
			
			$sql = "SELECT ".$this->sys['prefix']."info.id, ".$this->sys['prefix']."info.titulo, ".$this->sys['prefix']."info.descricao, ".$this->sys['prefix']."info.permalink FROM `".$this->sys['prefix']."info` WHERE snDestaque='S' ORDER BY id DESC LIMIT 10";
	
			$query = $this->mquery($sql);


			while ($result = mysql_fetch_object($query)) {
				$id = $result->id;
				$permalink = $result->permalink;
				$descricao = $result->descricao;
				
				$link = "/$id/$permalink";
				$imagem = "imagens/noticias/$id/foto_grande.jpg";
				
				$titulo = utf8_encode($result->titulo);
				
				$temp = str_replace("[IMAGEM-NOTICIA]", $imagem, $template);				
				$temp = str_replace("[LINK-NOTICIA]", $link, $temp);

				$temp = str_replace("[DESCRICAO-NOTICIA]", utf8_encode($descricao), $temp);
				
				$conteudo .= $temp;
			}

			$conteudo2 = str_replace("[REPLACE-TEMPLATE]", $conteudo, $template2);
			
			$this->write($detfile, $conteudo2);		
		}
		
		function generateIndexNoticias($alert = true) {
			$template_addr = "../../site/index_noticias_object_template.php";
			$detfile = "../../site/index_noticias_object.php";
			
			$template = file_get_contents($template_addr);
			
			$sql = "SELECT ".$this->sys['prefix']."info.id, ".$this->sys['prefix']."info.titulo, ".$this->sys['prefix']."info.permalink FROM `".$this->sys['prefix']."info` WHERE snDestaque = 'N' ORDER BY id DESC LIMIT 4";
			$query = $this->mquery($sql);
			
			while ($result = mysql_fetch_object($query)) {
				$id = $result->id;
				$permalink = $result->permalink;
				
				$link = "/$id/$permalink";
				
				$titulo = utf8_encode($result->titulo);
				
				$temp = str_replace("[LINK-NOTICIA]", $link, $template);
				$temp = str_replace("[TITULO-NOTICIA]", $titulo, $temp);
				
				$conteudo .= $temp;  
			}
			$this->write($detfile, $conteudo);		
		}
		
		function generateLateralEnquetes($alert = true) {
			$template_addr = "../../site/lateral_esquerda_enquetes_object_template.php";
			$detfile = "../../site/lateral_esquerda_enquetes_object.php";
			
			$template = file_get_contents($template_addr);
			
			$sql = "SELECT * FROM `".$this->sys['db']."`.`".$this->sys['prefix2']."info` WHERE active=1 ORDER BY id DESC LIMIT 0, 1";
			$query = $this->mquery($sql);
			
			while ($result = mysql_fetch_object($query)) {
				$id = $result->id;
				
				$titulo = utf8_encode($result->titulo);
				$opt1 = utf8_encode($result->opt1);
				$opt2 = utf8_encode($result->opt2);
				$opt3 = utf8_encode($result->opt3);
				
				$temp = str_replace("[TITULO-ENQUETE]", $titulo, $template);
				$temp = str_replace("[OPCAO1-ENQUETE]", $opt1, $temp);
				$temp = str_replace("[OPCAO2-ENQUETE]", $opt2, $temp);
				$temp = str_replace("[OPCAO3-ENQUETE]", $opt3, $temp);
				$temp = str_replace("[ID-ENQUETE]", $id, $temp);
				
				$conteudo .= $temp;  
			}
			$this->write($detfile, $conteudo);		
		}
		function generateLateralFaq($alert = true) {
			$template_addr = "../../site/lateral_direita_perguntas_object_template.php";
			$detfile = "../../site/lateral_direita_perguntas_object.php";
			
			$template = file_get_contents($template_addr);
			
			$sql = "SELECT * FROM `".$this->sys['db']."`.`".$this->sys['prefix3']."info` ORDER BY id DESC LIMIT 0, 2";
			$query = $this->mquery($sql);
			
			for ($n = 1; $result = mysql_fetch_object($query); $n++) {
				$id = $result->id;
				
				$titulo = utf8_encode($result->titulo);
				$permalink = utf8_encode($result->permalink);
				$id = utf8_encode($result->id);
				
				$link = "/$id/$permalink";
				
				$temp = str_replace("[TITULO-FAQ]", $titulo, $template);
				$temp = str_replace("[LINK-FAQ]", $link, $temp);
				
				$conteudo .= $temp;  
			}
			//echo $template_addr;
			$this->write($detfile, $conteudo);		
		}
		function generateInforme($alert = true) {
			$template_addr = "../../site/index_informe_object_template.php";
			$detfile = "../../site/index_informe_object.php";
			
			$template = file_get_contents($template_addr);
			
			$sql = "SELECT * FROM `".$this->sys['db']."`.`".$this->sys['prefix4']."info` ORDER BY id DESC LIMIT 0, 1";
			$query = $this->mquery($sql);
			
			for ($n = 1; $result = mysql_fetch_object($query); $n++) {
				$id = $result->id;
				
				$titulo = utf8_encode($result->titulo);
				$resumo = utf8_encode($result->descricao);
				$permalink = utf8_encode($result->permalink);
				$id = utf8_encode($result->id);
				
				$link = "/$id/$permalink";
				
				$temp = str_replace("[TITULO-CIENTIFICO]", $titulo, $template);
				$temp = str_replace("[RESUMO-CIENTIFICO]", $resumo, $temp);
				$temp = str_replace("[LINK-CIENTIFICO]", $link, $temp);
                                if(file_exists("http://laboratoriogaspar.com.br/site/imagens/informe/$id/foto_miniatura.jpg")){
                                    $temp = str_replace("[IMAGEM-COLUNA]", "<td width=\"192\" align=\"left\" valign=\"top\"><a href=\"http://laboratoriogaspar.com.br/site/informe[LINK-CIENTIFICO]\"><img src=\"http://laboratoriogaspar.com.br/site/imagens/informe/[ID-CIENTIFICO]/foto_miniatura.jpg\" width=\"183\" height=\"80\" class=\"borda_imagem\" /></a></td>",
                                            $temp);
                                }else{
                                    $temp = str_replace("[IMAGEM-COLUNA]", "", $temp);
                                }
				$temp = str_replace("[ID-CIENTIFICO]", $id, $temp);
				
				$conteudo .= $temp;  
			}
			//echo $template_addr;
			$this->write($detfile, $conteudo);		
		}
		
		function generateInformeList($alert = true) {
			$template_addr = "../../site/informe_ver_list_object_template.php";
			$detfile = "../../site/informe_ver_list_object.php";
			
			$template = file_get_contents($template_addr);
			
			$sql = "SELECT * FROM `".$this->sys['db']."`.`".$this->sys['prefix4']."info` ORDER BY id DESC LIMIT 0, 3";
			$query = $this->mquery($sql);
			
			for ($n = 1; $result = mysql_fetch_object($query); $n++) {
				$id = $result->id;
				
				$titulo = utf8_encode($result->titulo);
				$permalink = utf8_encode($result->permalink);
				$id = utf8_encode($result->id);
				
				$link = "/$id/$permalink";
				
				$temp = str_replace("[TITULO-CIENTIFICO]", $titulo, $template);
				$temp = str_replace("[LINK-CIENTIFICO]", $link, $temp);
				
				$conteudo .= $temp;  
			}
			//echo $template_addr;
			$this->write($detfile, $conteudo);		
		}
		
		
		function generateNovidades($alert = true) {
			$template_addr = "../../site/lateral_direita_novidades_object_template.php";
			$detfile = "../../site/lateral_direita_novidades_object.php";
			
			$template = file_get_contents($template_addr);
			
			$sql = "SELECT * FROM `".$this->sys['db']."`.`".$this->sys['prefix6']."info` ORDER BY id DESC LIMIT 0, 1";
			$query = $this->mquery($sql);
			
			for ($n = 1; $result = mysql_fetch_object($query); $n++) {
				$id = $result->id;
				
				$descricao = utf8_encode($result->descricao);
				$permalink = utf8_encode($result->permalink);
				$id = utf8_encode($result->id);
				
				$link = "/$id/$permalink";
				
				$temp = str_replace("[DESCRICAO-NOVIDADES]", $descricao, $template);
				$temp = str_replace("[LINK-NOVIDADES]", $link, $temp);
				$temp = str_replace("[ID-NOVIDADES]", $id, $temp);
				
				$conteudo .= $temp;  
			}
			//echo $template_addr;
			$this->write($detfile, $conteudo);		
		}
		
		function generateRedesSociais($alert = true) {
			$template_addr = "../../site/lateral_direita_redessociais_template.php";
			$detfile = "../../site/lateral_direita_redessociais_object.php";
			
			$template = file_get_contents($template_addr);
			
			$sql = "SELECT * FROM `".$this->sys['db']."`.`".$this->sys['prefix5']."info` LIMIT 0, 1";
			$query = $this->mquery($sql);
			
			for ($n = 1; $result = mysql_fetch_object($query); $n++) {
				$id = $result->id;
				
				$twitter = utf8_encode($result->twitter);
				$facebook = utf8_encode($result->facebook);
				$orkut = utf8_encode($result->orkut);
				$myspace = utf8_encode($result->myspace);
				
				
				$temp = str_replace("[TWITTER-LINK]", $twitter, $template);
				$temp = str_replace("[FACEBOOK-LINK]", $facebook, $temp);
				$temp = str_replace("[ORKUT-LINK]", $orkut, $temp);
				$temp = str_replace("[MYSPACE-LINK]", $myspace, $temp);
				
				$conteudo .= $temp;  
			}
			//echo $template_addr;
			$this->write($detfile, $conteudo);		
		}

		
		
		
		function tira_acentos($texto){
			$texto = utf8_decode($texto);
			$texto = eregi_replace("[����]","a",$texto);
			$texto = eregi_replace("[���]","e",$texto);
			$texto = eregi_replace("[���]","i",$texto);
			$texto = eregi_replace("[����]","o",$texto);
			$texto = eregi_replace("[���]","u",$texto);
			$texto = eregi_replace("[�]","c",$texto);
			$texto = eregi_replace("[#]","",$texto);
			$texto = str_replace("�", "c", $texto);
			return $texto;
		}

	}
	$adm_site = new adm_site;
?>