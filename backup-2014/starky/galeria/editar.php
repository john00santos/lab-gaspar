<?
	require '../scriptsforload.php';
	require 'classes/galeria.php';
	$galeria->loadValues($_GET['idFoto']);
	$origin = "index.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>
<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador .pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#paginas {
		float:none;
		clear:both;
	}
</style>
<div id="container">
<? require '../login.php'; ?>

    <div id="corpo">
<div class="titlearea">Editar foto da galeria</div>
<form name="form" method="post" action="actions.php?act=galeria_editphoto&idFoto=<? echo $_GET['idFoto'];?>&idGaleria=<?echo $_GET['idGaleria']?>&idPai=<?=$_GET['idPai']?>&owner=<?=$_GET['owner']?>" enctype="multipart/form-data"/>
<input type="hidden" name="MAX_FILE_SIZE" value="21474836480">
<input type="button" value="Voltar"
onclick="history.go(-1)"> </p>

<table width="800" border="0">
  <tr>
    <td width="135">Legenda:</td>
    <td width="678"><input type="text" name="fields[legenda]" class="text" value="<? echo $galeria->info['legenda']; ?>"/></td>
  </tr>
  <tr>
    <td width="135">Imagem:</td>
    <td width="678">
    <img src="<?=$galeria->info['src']?>" /><br />
    Para alterar, selecione um novo arquivo
    <input type="file" name="imagem" class="text"/>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="middle"><input type="submit" value="Enviar" onclick="saveall()" /></td>
    </tr>
</table>


</div>
</div>
</body>
</html>