<?
	require '../scriptsforload.php';
	require 'classes/galeria.php';
	$origin = "index.php";

	if(!$galeria->getIdGaleria($_GET['id'])){
		try {
			$galeria->add($_GET['id'], $_GET['owner']);	
		}catch(Exception $e){
			echo "Não sei que é o proprietário. Contate a ELO";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>
<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador .pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#paginas {
		float:none;
		clear:both;
	}
</style>
<div id="container">
<? require '../login.php'; ?>

    <div id="corpo">
<div class="titlearea">Adicionar fotos a galeria</div>
<form name="form" method="post" action="actions.php?act=galeria_addphoto&idPai=<? echo $_GET['id']; ?>&owner=<?=$_GET['owner']?>" enctype="multipart/form-data"/>
<input type="hidden" name="MAX_FILE_SIZE" value="21474836480">
<table width="800" border="0">
  <tr>
    <td width="135">Legenda:</td>
    <td width="678"><input type="text" name="fields[legenda]" class="text" value=""/></td>
  </tr>  
  <tr>
    <td width="135">Imagem:</td>
    <td width="678"><input type="file" name="imagem" class="text"/>
    </td>
  </tr>

  <tr>
    <td colspan="2" align="center" valign="middle"><input type="submit" value="Enviar" onclick="saveall()" /></td>
    </tr>
</table>
<div class="titlearea">Fotos da Galeria</div>
<div id="thumbsarea">
	<? $galeria->loadGaleria($_GET['id'], $_GET['owner']); ?>
</div>

</div>
</div>
</body>
</html>
