<textarea name="fields[dsTexto]" id="texto" cols="50" rows="10"><?=utf8_encode($adm_conteudos->loadTexto($adm_conteudos->fields['idConteudo']));?>
</textarea>
<script type="text/javascript">
	var confResumo = {
		height: '200px',
		width: '100%',
		dompath: false,
		animate: true,
		toolbar: {
			collapse: true,
			titlebar: 'Resumo',
			draggable: false,
			buttonType: 'advanced',
			buttons: [
				{ group: 'fontstyle', label: 'Fontes e Tamanhos',
					buttons: [
						{ type: 'select', label: 'Arial', value: 'fontname', disabled: true,
							menu: [
								{ text: 'Arial', checked: true },
								{ text: 'Arial Black' },
								{ text: 'Comic Sans MS' },
								{ text: 'Courier New' },
								{ text: 'Lucida Console' },
								{ text: 'Tahoma' },
								{ text: 'Times New Roman' },
								{ text: 'Trebuchet MS' },
								{ text: 'Verdana' }
							]
						},
						{ type: 'spin', label: '13', value: 'fontsize', range: [ 9, 75 ], disabled: true }
					]
				},
				{ type: 'separator' },
				{ group: 'textstyle', label: 'Estilizar Fonte',
					buttons: [
						{ type: 'push', label: 'Bold CTRL + SHIFT + B', value: 'bold' },
						{ type: 'push', label: 'Italic CTRL + SHIFT + I', value: 'italic' },
						{ type: 'push', label: 'Underline CTRL + SHIFT + U', value: 'underline' },
						{ type: 'separator' },
						{ type: 'color', label: 'Font Color', value: 'forecolor', disabled: true },
						{ type: 'color', label: 'Background Color', value: 'backcolor', disabled: true },
						{ type: 'separator' },
						{ type: 'push', label: 'Remove Formatting', value: 'removeformat', disabled: true },
					]
				}				
			]
		}	
	}
	var confTexto = {
		height: '300px',
		width: '100%',
		dompath: false,
		animate: true,
		toolbar: {
			collapse: true,
			titlebar: 'Texto',
			draggable: false,
			buttonType: 'advanced',
			buttons: [
				{ group: 'fontstyle', label: 'Fontes e Tamanhos',
					buttons: [
						{ type: 'select', label: 'Arial', value: 'fontname', disabled: true,
							menu: [
								{ text: 'Arial', checked: true },
								{ text: 'Arial Black' },
								{ text: 'Comic Sans MS' },
								{ text: 'Courier New' },
								{ text: 'Lucida Console' },
								{ text: 'Tahoma' },
								{ text: 'Times New Roman' },
								{ text: 'Trebuchet MS' },
								{ text: 'Verdana' }
							]
						},
						{ type: 'spin', label: '13', value: 'fontsize', range: [ 9, 75 ], disabled: true }
					]
				},
				{ type: 'separator' },
				{ group: 'textstyle', label: 'Estilizar Fonte',
					buttons: [
						{ type: 'push', label: 'Bold CTRL + SHIFT + B', value: 'bold' },
						{ type: 'push', label: 'Italic CTRL + SHIFT + I', value: 'italic' },
						{ type: 'push', label: 'Underline CTRL + SHIFT + U', value: 'underline' },
						{ type: 'separator' },
						{ type: 'push', label: 'Subscript', value: 'subscript', disabled: true },
						{ type: 'push', label: 'Superscript', value: 'superscript', disabled: true },
						{ type: 'separator' },
						{ type: 'color', label: 'Font Color', value: 'forecolor', disabled: true },
						{ type: 'color', label: 'Background Color', value: 'backcolor', disabled: true },
						{ type: 'separator' },
						{ type: 'push', label: 'Remove Formatting', value: 'removeformat', disabled: true },
					]
				},
				{ type: 'separator' },
				{ group: 'alignment', label: 'Alinhamento',
					buttons: [
						{ type: 'push', label: 'Align Left CTRL + SHIFT + [', value: 'justifyleft' },
						{ type: 'push', label: 'Align Center CTRL + SHIFT + |', value: 'justifycenter' },
						{ type: 'push', label: 'Align Right CTRL + SHIFT + ]', value: 'justifyright' },
						{ type: 'push', label: 'Justify', value: 'justifyfull' }
					]
				},
				{ type: 'separator' },
				{ group: 'parastyle', label: 'Parágrafo',
					buttons: [
					{ type: 'select', label: 'Normal', value: 'heading', disabled: true,
						menu: [
							{ text: 'Normal', value: 'none', checked: true },
							{ text: 'Header 1', value: 'h1' },
							{ text: 'Header 2', value: 'h2' },
							{ text: 'Header 3', value: 'h3' },
							{ text: 'Header 4', value: 'h4' },
							{ text: 'Header 5', value: 'h5' },
							{ text: 'Header 6', value: 'h6' }
						]
					}
					]
				},
				{ type: 'separator' },
				{ group: 'indentlist', label: 'Listas',
					buttons: [
						{ type: 'push', label: 'Create an Unordered List', value: 'insertunorderedlist' },
						{ type: 'push', label: 'Create an Ordered List', value: 'insertorderedlist' }
					]
				},
				{ type: 'separator' },
				{ group: 'insertitem', label: 'Inserir',
					buttons: [
						{ type: 'push', label: 'HTML Link CTRL + SHIFT + L', value: 'createlink', disabled: true },
						{ type: 'push', label: 'Inserir Imagem', value: 'insertimage' }
					]
				}
			]
		}	
	}

	var fieldTexto = new YAHOO.widget.Editor('texto', confTexto);
	fieldTexto.render();
</script>