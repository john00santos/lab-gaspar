<?


	class adm_novidades {
		
		public $fields;
		
		static private $sys;	
		
		private $info;		
		
		public function __construct() {
			
			$this->sys['db'] = "gasparhp";
			$this->sys['prefix'] = "novidades_";
			
			if ($_SERVER['REMOTE_ADDR'] != "127.0.0.1") {
				$host = "sqlsrv.elo.com.br";
				$user = "gasparhp";
				$pass = "1oPYGsaL";
			}
			else {
				$host = "localhost";
				$user = "root";
				$pass = "";
			}
			
			$this->info['conn'] = mysql_connect($host, $user, $pass) or die ("Erro ao conectar");
			mysql_select_db($this->sys['db'], $this->info['conn']) ;
			
			session_start();
		}
		
		
		private function mquery($sql) {
			$query = mysql_query($sql, $this->info['conn']) or die (mysql_error($query) . " $sql");
			return ($query);
		}
		private function validar($string) {
			return mysql_real_escape_string($string);	
		}
		
		public function getLastValues() {
			$sql = "SELECT id FROM `".$this->sys['db']."`.`".$this->sys['prefix']."info` ORDER BY id DESC LIMIT 0, 1";
			$query = $this->mquery($sql);
			if (mysql_num_rows($query) > 0) {
				$result = mysql_fetch_object($query);
				$nextid = $result->id + 1;
			}
			else {
				$nextid = 0;	
			}
			$this->fields['nextid'] = $nextid;
		}
		
		public function loadValues($id) {
			$sql = "SELECT ".$this->sys['prefix']."info.titulo, ".$this->sys['prefix']."info.subtitulo, ".$this->sys['prefix']."info.descricao, ".$this->sys['prefix']."info.texto_twitter, ".$this->sys['prefix']."info.resumo, ".$this->sys['prefix']."info.categoria, ".$this->sys['prefix']."info.data, ".$this->sys['prefix']."info.data_extenso, ".$this->sys['prefix']."info.hora, ".$this->sys['prefix']."info.tags, ".$this->sys['prefix']."pages.texto FROM `".$this->sys['db']."`.`".$this->sys['prefix']."info`, `".$this->sys['db']."`.`".$this->sys['prefix']."pages` WHERE ".$this->sys['prefix']."info.id='$id' and ".$this->sys['prefix']."pages.info_id='$id'";
			$query = $this->mquery($sql);
			if (mysql_num_rows($query) > 0) {
				$result = mysql_fetch_object($query);
				
				//Jogando o resultado da pesquisa para variável matriz fields
				
				$this->fields['titulo'] = $titulo = utf8_encode($result->titulo);
				$this->fields['subtitulo'] = $subtitulo = utf8_encode($result->subtitulo);
				$this->fields['descricao'] = $descricao = utf8_encode($result->descricao);
				$this->fields['texto_twitter'] = $texto_twitter = utf8_encode($result->texto_twitter);
				$this->fields['categoria'] = $categoria = utf8_encode($result->categoria);
				$this->fields['tags'] = $tags = utf8_encode($result->tags);
				$this->fields['resumo'] = $resumo = utf8_encode($result->resumo);
				$this->fields['texto'] = $texto = utf8_encode($result->texto);
				
				
			}
		}
		
		public function editValues($id, $titulo, $subtitulo, $descricao, $texto_twitter, $data, $data_extenso, $tags, $resumo, $texto, $permalink, $bitly) {
						
			$asql = "UPDATE `".$this->sys['db']."`.`".$this->sys['prefix']."info`, `".$this->sys['db']."`.`".$this->sys['prefix']."pages` SET ".$this->sys['prefix']."info.titulo='".$titulo."', ".$this->sys['prefix']."info.descricao='".$descricao."', ".$this->sys['prefix']."info.subtitulo='".$subtitulo."', ".$this->sys['prefix']."info.categoria='".$categoria."', ".$this->sys['prefix']."info.data='".$data."', ".$this->sys['prefix']."info.data_extenso='".$data_extenso."', ".$this->sys['prefix']."info.tags='".$tags."', ".$this->sys['prefix']."info.resumo='".$resumo."', ".$this->sys['prefix']."info.texto_twitter='".$texto_twitter."', ".$this->sys['prefix']."pages.texto='".$texto."', ".$this->sys['prefix']."info.permalink='".$permalink."', ".$this->sys['prefix']."info.bitly='".$bitly."' WHERE ".$this->sys['prefix']."info.id='".$id."' and ".$this->sys['prefix']."pages.info_id='".$id."'";
			$query = $this->mquery($asql);	
			
		}
		
		public function addValues($id, $titulo, $subtitulo, $descricao, $texto_twitter, $data, $data_extenso, $tags, $resumo, $texto, $permalink, $bitly) {
			
			
			
			$asql = "INSERT INTO `".$this->sys['db']."`.`".$this->sys['prefix']."info` (`id`, `titulo`, `subtitulo`, `descricao`, `texto_twitter`, `data`, `data_extenso`, `tags`, `resumo`, `permalink`, `bitly`) VALUES ($id, '".$titulo."', '".$subtitulo."', '".$descricao."', '".$texto_twitter."', '".$data."', '".$data_extenso."', '".$tags."', '".$resumo."', '".$permalink."', '".$bitly."');";
			$aquery = $this->mquery($asql);
			unset($aquery);
                            $id = mysql_insert_id();
			
			$csql = "INSERT INTO `".$this->sys['db']."`.`".$this->sys['prefix']."pages` (`secao`, `info_id`, `texto`) VALUES (1, '".$id."', '".$texto."');";
			echo $csql;
			$cquery = $this->mquery($csql);
			unset($cquery);
			
			return $id;
				
		}
		
		function delete($id) {
			$sql = "DELETE FROM `".$this->sys['db']."`.`".$this->sys['prefix']."info` WHERE id='$id'";
			$this->mquery($sql);
			$sql = "DELETE FROM `".$this->sys['db']."`.`".$this->sys['prefix']."pages` WHERE info_id='$id'";
			$this->mquery($sql);
			
		}
		
		function getNavNextVars() {
			if (!isset($_GET['order'])) {
				$_GET['order'] = "DESC";
			}
			$pg = $_GET['pg'];
			$ipp = $_GET['ipp'];
			if ((!isset($ipp)) || ($ipp < 10)) {
				$ipp = 10 ;
			}
			if ($pg == 0) {
				$pg2 = $ipp;
			}
			else {
				$pg2 = $pg + $ipp;
			}
			$sql = "SELECT * FROM `".$this->sys['db']."`.`".$this->sys['prefix']."info` ORDER BY id DESC LIMIT $pg2, 1";
			$query = $this->mquery($sql);
			if (mysql_num_rows($query) > 0) {
				$this->nav["next"] = true;
			}
			else {
				$this->nav["next"] = false;
			}
			$this->pages["next"] = $pg2;
		}
		function getNavBackVars() {
			$pg = $_GET['pg'];
			$ipp = $_GET['ipp'];
			if ((!isset($ipp)) || ($ipp < 10)) {
				$ipp = 10 ;
			}
			$pg2 = $pg - $ipp;
			if ($pg2 < 0) {
				$pg2 = 0;
			}
			if ($pg < $ipp) {			
				$this->nav["back"] = false;
			}
			elseif ($pg >= $ipp) {
				$this->nav["back"] = true;
			}
			$this->pages["back"] = $pg2;
		}
		function listitens() {
			if (!isset($_GET['order'])) {
				$_GET['order'] = "DESC";
			}
			$pg = $_GET['pg'];
			$ipp = $_GET['ipp'];
			if ((!isset($pg)) || ($pg < 0) || ($pg == "")) {
				$pg = 0 ;
			}
			if ((!isset($ipp)) || ($ipp < 10) || ($ipp == "")) {
				$ipp = 10 ;
			}

			$sql = "SELECT * FROM `".$this->sys['db']."`.`".$this->sys['prefix']."info` ORDER BY id DESC LIMIT $pg, $ipp";
			$query = $this->mquery($sql);
			while ($result = mysql_fetch_object($query)) {
			
				require 'listar_object.php';		
	
			}
		}
		function paginacao() {
			$this->getNavNextVars();
			$this->getNavBackVars();
			if ($this->nav["back"]) echo '<a href="?pg='. $this->pages["back"] .'&ipp='. $_GET['ipp'].'">&lt;Anterior</a>';
			if (($this->nav["back"]) && ($this->nav["next"])) echo " | ";
			if ($this->nav["next"]) echo '<a href="?pg='. $this->pages["next"] .'&ipp='. $_GET['ipp'].'">Pr&oacute;xima&gt;</a>';
		}
		
		
		function adjustEncoding() {
			$sql = "SELECT ".$this->sys['prefix']."info.titulo, ".$this->sys['prefix']."info.id, ".$this->sys['prefix']."info.subtitulo, ".$this->sys['prefix']."info.resumo, ".$this->sys['prefix']."pages.texto FROM `".$this->sys['db']."`.`".$this->sys['prefix']."info`, `".$this->sys['db']."`.`".$this->sys['prefix']."pages` WHERE ".$this->sys['prefix']."info.id < 21;";
			$query = mysql_query($sql, $this->novidadesConn);
			while ($result = mysql_fetch_object($query)) {
				$id = $result->id;
				$titulo = utf8_encode($result->titulo);
				$texto = utf8_encode($result->texto);
				$subtitulo = utf8_encode($result->subtitulo);
				$resumo = utf8_encode($result->resumo);
				
				$nsqla = "UPDATE `".$this->sys['db']."`.`".$this->sys['prefix']."info` SET titulo='$titulo', subtitulo='$subtitulo', resumo='$resumo' WHERE id='$id'; ";
				$nsqlb = "UPDATE `".$this->sys['db']."`.`".$this->sys['prefix']."pages` SET texto='$texto' WHERE info_id='$id'; ";
				$nsql = $nsqla.$nsqlb;
				echo $nsql."<br>";
				$nquery = mysql_query($nsql, $this->novidadesConn);
			}
		}
		
		function definirDestaque($cat, $id) {
			$sql = "UPDATE `".$this->sys['db']."`.destaques SET id='$id' WHERE categoria='$cat'";
			$query = mysql_query($sql, $this->novidadesConn);
			?>
				<script language="javascript">
					var url = "listar.php?cat=<? echo $cat; ?>";
					document.location.href=url;
				</script>
			<? 
		}

		
		
		
		public function __destruct() {
			@mysql_close($this->sys['db']);
		}
	}
	$adm_novidades = new adm_novidades;

?>