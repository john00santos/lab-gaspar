<?
	require_once 'classes/novidades.php';

	
	function tratarImgs($id) {
		require_once '../classes/redutor.php';
		$pastaImgRoot = $_SERVER['DOCUMENT_ROOT']."/site/imagens/novidades/".$id."/";
		@mkdir($pastaImgRoot);
		/*$tempfile = $pastaImgRoot."temp_pequena.jpg";
		if (move_uploaded_file($_FILES['imagempequena']['tmp_name'], $tempfile)) {
			$origem = $tempfile;
			$destino = str_replace("temp_pequena.jpg", "foto_miniatura.jpg", $tempfile);
			$dest_width = 131;
			$dest_height = 91;
			$max_width = 1000;
			$max_height = 91;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);	
			unlink($tempfile);
		}

		$tempfile = $pastaImgRoot."temp_grande.jpg";	
		if (move_uploaded_file($_FILES['imagemgrande']['tmp_name'], $tempfile)) {				
			$origem = $tempfile;
			$destino = str_replace("temp_grande.jpg", "foto_grande.jpg", $tempfile);
			$dest_width = 166;
			$dest_height = 221;
			$max_width = 1000;
			$max_height = 221;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);
			unlink($tempfile);
		}*/
        
                  $tempfile = $pastaImgRoot."foto_miniatura.jpg";
                if(!empty($_FILES['imagempequena']['tmp_name'])){
                    @unlink($tempfile);
                    move_uploaded_file($_FILES['imagempequena']['tmp_name'], $tempfile);
                }
                
                $tempfile = $pastaImgRoot."foto_grande.jpg";
                if(!empty($_FILES['imagemgrande']['tmp_name'])){
                    @unlink($tempfile);                                
                    move_uploaded_file($_FILES['imagemgrande']['tmp_name'], $tempfile);
                }
	}
	
	
	function trataDados() {
		
		require_once '../classes/textActions.php';
		require_once '../classes/dater.php';
		require_once '../classes/permalinks.php';
		
		$fields['titulo'] = $titulo = utf8_decode($_POST['fields']['titulo']);
		$fields['subtitulo'] = $subtitulo = utf8_decode($_POST['fields']['subtitulo']);
		$fields['descricao'] = $descricao = utf8_decode($_POST['fields']['descricao']);
		$fields['texto_twitter'] = $texto_twitter = utf8_decode($_POST['fields']['texto_twitter']);
		
		$fields['data'] = $data = date("Y-m-d H:m:s");
		$fields['data_extenso'] = $data_extenso = $dater->datar(date("Y-n-d"));
		$fields['tags'] = $tags = utf8_decode($_POST['fields']['tags']);
		
		$fields['resumo'] = $resumo = utf8_decode(str_replace("'", "\'", $textActions->strTrimTotal($_POST['ftexts']['1'])));
		$fields['texto'] = $texto = utf8_decode(str_replace("'", "\'", $textActions->strTrimTotal($_POST['ftexts']['2'])));
		
		$fields['permalink'] = $permalink = $permalinks->generate($_POST['fields']['titulo']);
		
		$fields['id'] = $id = $_POST['nextid'];
		
		$bitlyurl = "http://laboratoriogaspar.com.br/starky/objects/bitly/bitly_generator.php?url=".urlencode("http://laboratoriogaspar.com.br/site/novidades/".$id."/".$permalink);
		$fields['bitly'] = $bitly = file_get_contents($bitlyurl);
		
		return $fields;
	}
	
		
	switch($_GET['act']) {
		case "criar":			
			
			$fields = trataDados();		
			
			
			$id = $adm_novidades->addValues($fields['id'], $fields['titulo'], $fields['subtitulo'], $fields['descricao'], $fields['texto_twitter'], $fields['data'], $fields['data_extenso'], $fields['tags'], $fields['resumo'], $fields['texto'], $fields['permalink'], $fields['bitly']);
            
                           if(file_exists($_SERVER['DOCUMENT_ROOT']."site/imagens/novidades/".$id."/foto_miniatura.jpg"))
                                @unlink($_SERVER['DOCUMENT_ROOT']."site/imagens/novidades/".$id."/foto_miniatura.jpg");
                           if(file_exists($_SERVER['DOCUMENT_ROOT']."site/imagens/novidades/".$id."/foto_grande.jpg"))
                                @unlink($_SERVER['DOCUMENT_ROOT']."site/imagens/novidades/".$id."/foto_grande.jpg");
                           
			tratarImgs($fields['id']);
			
			
			?>
            	<script>
					alert("Post adicionado com sucesso.");
					document.location.href="listar.php";
					
				</script>
            <?
			break;
		case "editar":
					
			$fields = trataDados();	
			
			$adm_novidades->editValues($fields['id'], $fields['titulo'], $fields['subtitulo'], $fields['descricao'], $fields['texto_twitter'], $fields['data'], $fields['data_extenso'], $fields['tags'], $fields['resumo'], $fields['texto'], $fields['permalink'], $fields['bitly']);
			tratarImgs($fields['id']);
	
			?>
            	<script>
					alert("Post editado com sucesso.");
					document.location.href="listar.php";
				</script>
            <?
			break;
		case "deletar":
			$adm_novidades->delete($_GET['id']);
			?>
            	<script>
					alert("Post deletado com sucesso");
					document.location.href="listar.php";
				</script>
            <?
			break;
		case "adjustEncoding":
			$adm_novidades->adjustEncoding();
			break;
		case "definirdestaque":
			$adm_novidades->definirDestaque($cat, $id);
			break;
	}
	//header("Location: http://".$_GET['host']."/bandaxavecodemenina.com.br/starky/novidades/listar.php?ipp=".$_GET['ipp']."&pg=".$_GET['pg']);
?>
