<?
	require '../scriptsforload.php';
	require 'classes/banners.php';	
	$adm_banners->getLastValues();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>



<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador #pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#paginas {
		float:none;
		clear:both;
	}
</style>
<script src="../objects/textcounter/js/textcounter.js" type="text/javascript" language="javascript"></script>
<script src="../objects/bitly/js/bitly.js" type="text/javascript" language="javascript"></script>
<script src="../filesmanager/js/basic_functions.js" type="text/javascript" language="javascript"></script>
<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	
<? require 'menu.php'; ?>

<div id="corpo">
<div class="titlearea">Criar Banner</div>
<form name="form" method="post" action="actions.php?act=criar&host=<? echo $prop['host']; ?>" enctype="multipart/form-data"/>

<input type="hidden" name="MAX_FILE_SIZE" value="21474836480">
<input type="hidden" name="nextid" value="<? echo $adm_banners->fields['nextid']; ?>" />




<h4>Título:</h4>
<input type="text" name="fields[titulo]" maxlength="70" class="text_titulo" value="<? echo $paginas->fields['title']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<small>Máx: 70 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />


<h4>Link:</h4>
<input type="text" name="fields[link]" class="text_titulo" value="<? echo $adm_banners->fields['tags']; ?>"/><br />
<small>Insira o link completo, não se esquecendo do HTTP://</small>
<br />
<br />


<h4>Imagem:</h4>
<input type="file" name="imagem" class="text"/><br />
<small>Resolução: 442 x 157px</small>
<br />
<br />


<input type="submit" value="Enviar" />

<div id="control"></div>						
</form>

</div>
</body>
</html>