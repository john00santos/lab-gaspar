<?
	require '../scriptsforload.php';
//	require 'classes/interna.php';	
	require 'classes/interna.php';	
	$origin = "listar_remote.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>
<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador #pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#paginas {
		float:none;
		clear:both;
	}
</style>
<script src="js/richtext.js" type="text/javascript" language="javascript"></script>
<script src="js/config.js" type="text/javascript" language="javascript"></script>
<script src="js/paginacao.js" type="text/javascript" language="javascript"></script>
<script src="js/basic_functions.js" type="text/javascript" language="javascript"></script>
<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	
<? require 'menu.php'; ?>

    <div id="corpo">
	<div class="titlearea">Listar Notícias</div>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		  <!--DWLayoutTable-->
		  <tr>
			<td width="50%" height="30" align="left" valign="middle">	
			</td>
            <td width="50%" height="30" align="left" valign="middle">
				<span class="titulo3">Itens por página:</span> 
				<select name="viewed" onchange="goto('listar.php?cat=<? echo $_GET['categoria']; ?>ipp='+this.value')">
					<option>Selecione</option>
					<option>------</option>
					<option value="3">3</option>
					<option value="5">5</option>
					<option value="10">10</option>
				</select>		
			</td>
		  </tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <!--DWLayoutTable-->
			  <tr>
				<td width="73%" height="30" align="left" valign="middle">
					<h2>Item</h2>				</td>
				<td width="27%" height="30" align="left" valign="middle">
					<h2>A&ccedil;&otilde;es</h2>				</td>
			  </tr>
			 <?
			 	$adm_interna->listitens($_GET['cat']);
			 ?>
			</table>
		<div align="center">
			<? $adm_interna->paginacao(); ?>
		</div>
	</div>    
</body>
</html>
