<?
	require 'scriptsforload.php';
	$origin = "index.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>

<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	

    <div id="corpo">
    	<? if ($_SESSION['user_at']) { ?>
        	<script>
				document.location.href = '../interna/';
			</script>
        <? } ?>
    </div>
</div>    
</body>
</html>