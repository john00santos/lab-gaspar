<?
	require '../scriptsforload.php';
	require 'classes/usuarios.php';
	$usuarios->getLastValues();
	$usuarios->loadValues($_GET['id']);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>



<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador #pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#usuarios {
		float:none;
		clear:both;
	}
</style>
<script src="../objects/textcounter/js/textcounter.js" type="text/javascript" language="javascript"></script>
<script src="../objects/bitly/js/bitly.js" type="text/javascript" language="javascript"></script>
<script src="../objects/password/js/password.js" type="text/javascript" language="javascript"></script>
<script src="../filesmanager/js/basic_functions.js" type="text/javascript" language="javascript"></script>
<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	
<? require 'menu.php'; ?>

<div id="corpo">
<div class="titlearea">Editar Usuário</div>
<form name="form" method="post" action="actions.php?host=<? echo $prop['host']; ?>&act=editar&id=<? echo $_GET['id']; ?>" enctype="multipart/form-data"/>

<input type="hidden" name="MAX_FILE_SIZE" value="21474836480">
<input type="hidden" name="nextid" value="<? echo $_GET['id']; ?>" />




<h4>Nome:</h4>
<input type="text" name="fields[nome]" maxlength="70" class="text_titulo" value="<? echo $usuarios->fields['nome']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />

<h4>Cargo:</h4>
<input type="text" name="fields[cargo]" maxlength="70" class="text_titulo" value="<? echo $usuarios->fields['cargo']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />

<h4>E-mail:</h4>
<input type="text" name="fields[email]" maxlength="70" class="text_titulo" value="<? echo $usuarios->fields['email']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />

<h4>Login:</h4>
<input type="text" name="fields[login]" maxlength="70" class="text_titulo" value="<? echo $usuarios->fields['login']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<small>Máx: 20 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />

<h4>Senha:</h4>
<input type="text" id="senha" name="fields[senha]" maxlength="10" class="text_titulo" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<input type="button" value="Gerar senha" onclick="document.getElementById('senha').value = Passgenerate(10)" />
<small>Deixe em branco se não quiser alterar</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />

<h4>Nível:</h4>
<select name="fields[level]">
	<option value="9">Nível 9 - ROOT</option>
    <option value="5">Nível 5 - ASSESSORES</option>
    <option value="1">Nível 1 - OUTROS</option>
</select>
<small></small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />


<h4>Imagem Pequena:</h4>
<img src="http://laboratoriogaspar.com.br/starky/usuarios/images/perfis/<? echo $_GET['id']; ?>/foto_miniatura.jpg" /><br />
<small>Para alterar, envie uma nova abaixo:</small><br />
<input type="file" name="imagempequena" class="text"/><br />
<small>Resolução: 166 x 221px</small>
<br />
<br />

<input type="submit" value="Enviar" />

<div id="control"></div>						
</form>

</div>
</body>
</html>