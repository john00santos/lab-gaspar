<?
	$str = '<p>Após meses de pesquisa, a equipe do Portal EnfOKO.com.br desenvolveu uma nova tecnologia para oferecer aos nossos vistantes uma nova experiência em coberturas de festas. Esta nova tecnologia permitirá aos usuários ter fotos em Alta Definição em formato 16:9 Widescreen, bem maiores que as oferecidas anteriormente (4:3 SD). Logo, uma maior riqueza de detalhes estarão presentes nas fotos, um &quot;brilho especial&quot; e cores mais vivas.</p>
<p>Para que isso fosse possível, foi necessário redesenhar a galeria de fotos, para que ela pudesse suportar o novo esquema de tamanhos. Com o novo desenho, ficou ainda mais fácil de comentar as fotos - parecido com o orkut -, visualizar as miniaturas e o melhor é que ela é otimizada para a utilização em monitores Widescreen (16:10). Você pode experimentar visualizar as suas fotos apertando <strong>F11</strong> na galeria, deixando ela em tela cheia.</p>
<p>Ainda não inciamos oficialmente a utilização desta tecnologia, mas já foi experimentada em 3 eventos recentes, como o Choppósio Literário, Blitz IBNS e o Aniversário de Manuella e Vitinho Garam, com muitas fotos tiradas em 16:9.</p>
<p>A previsão é que seja oficialmente lançada no evento <a href="http://enfoko.com.br/beta2/eventos/detalhes.php?id=96">&quot;Forrogode&quot;</a>, da Auê Produções no Iate Clube, dia 06 de novembro.</p>
<p>Aproveite essa novidade!<br />
Tire fotos em Alta Definição conosco!</p>
<p>&nbsp;</p>
<p>Postado por: Tagory Cardoso</p>
';
	echo utf8_decode($str);
?>