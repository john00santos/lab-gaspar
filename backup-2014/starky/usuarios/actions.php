<?
	require_once 'classes/usuarios.php';

	
	function tratarImgs($id) {
		require_once '../classes/redutor.php';
		$pastaImgRoot = $_SERVER['DOCUMENT_ROOT']."/starky/usuarios/images/perfis/".$id."/";
		@mkdir($pastaImgRoot);
		$tempfile = $pastaImgRoot."temp_pequena.jpg";
		if (move_uploaded_file($_FILES['imagempequena']['tmp_name'], $tempfile)) {
			$origem = $tempfile;
			$destino = str_replace("temp_pequena.jpg", "foto_miniatura.jpg", $tempfile);
			$dest_width = 166;
			$dest_height = 221;
			$max_width = 1000;
			$max_height = 221;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);	
			unlink($tempfile);
		}

	}
	
	
	function trataDados() {
		
		$fields['id'] = $id = $_POST['nextid'];
		$fields['nome'] = $nome = utf8_decode($_POST['fields']['nome']);
		$fields['cargo'] = $cargo = utf8_decode($_POST['fields']['cargo']);
		$fields['email'] = $email = utf8_decode($_POST['fields']['email']);
		$fields['login'] = $login = base64_encode(utf8_decode($_POST['fields']['login']));
		$fields['senha'] = $senha = base64_encode(utf8_decode($_POST['fields']['senha']));
		$fields['level'] = $level = utf8_decode($_POST['fields']['level']);
		
		
		return $fields;
	}
	
		
	switch($_GET['act']) {
		case "criar":			
			
			$fields = trataDados();		
			
			
			$usuarios->addValues($fields['id'], $fields['nome'], $fields['cargo'], $fields['email'], $fields['login'], $fields['senha'], $fields['level']);
			tratarImgs($fields['id']);
			
			
			?>
            	<script>
					alert("Usuário adicionado com sucesso");
					document.location.href="listar.php";					
				</script>
            <?
			break;
		case "editar":
					
			$fields = trataDados();	
			
			//echo $fields['texto_twitter'];
			
			$usuarios->editValues($fields['id'], $fields['nome'], $fields['cargo'], $fields['email'], $fields['login'], $fields['senha'], $fields['level']);
			tratarImgs($fields['id']);
	
			?>
            	<script>
					alert("Usuário editado com sucesso");
					document.location.href="listar.php";
				</script>
            <?
			break;
		case "deletar":
			$usuarios->delete($_GET['id']);
			?>
            	<script>
					alert("Usuário deletado com sucesso");
					document.location.href="listar.php";
				</script>
            <?
			break;

	}
	//header("Location: http://".$_GET['host']."/bandaxavecodemenina.com.br/starky/paginas/listar.php?ipp=".$_GET['ipp']."&pg=".$_GET['pg']);
?>