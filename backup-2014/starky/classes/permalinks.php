<?
	class permalinks {
		public function generate($texto){
			$texto = strtolower($texto);
			$texto = str_replace(".", "", $texto);
			$texto = str_replace(" ", "-", $texto);
			$texto = str_replace("á", "a", $texto);
			$texto = str_replace("â", "a", $texto);
			$texto = str_replace("à", "a", $texto);
			$texto = str_replace("ã", "a", $texto);
			$texto = str_replace("é", "e", $texto);
			$texto = str_replace("è", "e", $texto);
			$texto = str_replace("ê", "e", $texto);
			$texto = str_replace("í", "i", $texto);
			$texto = str_replace("ì", "i", $texto);
			$texto = str_replace("î", "i", $texto);
			$texto = str_replace("ó", "o", $texto);
			$texto = str_replace("ò", "o", $texto);
			$texto = str_replace("õ", "o", $texto);
			$texto = str_replace("ô", "o", $texto);
			$texto = str_replace("ú", "u", $texto);
			$texto = str_replace("ù", "u", $texto);
			$texto = str_replace("û", "u", $texto);
			$texto = str_replace("ç", "c", $texto);
			$texto = str_replace("#", "", $texto);
			$texto = str_replace("!", "", $texto);
			$texto = str_replace("?", "", $texto);
			$texto = str_replace("@", "", $texto);
			$texto = str_replace("$", "", $texto);
			$texto = str_replace(":", "", $texto);

			$texto = str_replace("Á", "a", $texto);
			$texto = str_replace("Â", "a", $texto);
			$texto = str_replace("À", "a", $texto);
			$texto = str_replace("Â", "a", $texto);
			$texto = str_replace("É", "e", $texto);
			$texto = str_replace("È", "e", $texto);
			$texto = str_replace("Ê", "e", $texto);
			$texto = str_replace("Í", "i", $texto);
			$texto = str_replace("Ì", "i", $texto);
			$texto = str_replace("Î", "i", $texto);
			$texto = str_replace("Ó", "o", $texto);
			$texto = str_replace("Ò", "o", $texto);
			$texto = str_replace("Õ", "o", $texto);
			$texto = str_replace("Ô", "o", $texto);
			$texto = str_replace("Ú", "u", $texto);
			$texto = str_replace("Ù", "u", $texto);
			$texto = str_replace("Û", "u", $texto);
			$texto = str_replace("Ç", "c", $texto);

			return $texto . ".html"; 
		}
	}
	$permalinks = new permalinks();
?>
