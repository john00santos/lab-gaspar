﻿<?
	require 'redutor.php';
	$action = $_POST['action'];
	
	if ($action == 'reduzir') {
		if (($_SERVER['REMOTE_ADDR'] != "127.0.0.1") && ($camadaip != "192.168.100")) {
			$pastaImgRoot = $_SERVER['DOCUMENT_ROOT']."/magazine/negocios/images/posted/$id/";
		}
		elseif (($camadaip == "192.168.100") || ($_SERVER['REMOTE_ADDR'] == "127.0.0.1"))  {
			$pastaImgRoot = $_SERVER['DOCUMENT_ROOT']."/grupoeinstein.com/starky/classes/teste/";
		}
		$tempfile = $pastaImgRoot."temp.jpg";
	
		if (move_uploaded_file($_FILES['imagem']['tmp_name'], $tempfile)) {
			
			$origem = $tempfile;
			$destino = str_replace("temp.jpg", "foto_capa.jpg", $tempfile);
			$dest_width = 700;
			$dest_height = 200;
			$max_width = 700;
			$max_height = 200;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);	

			$destino = str_replace("temp.jpg", "foto_miniatura.jpg", $tempfile);
			$dest_width = 100;
			$dest_height = 100;
			$max_width = 1000;
			$max_height = 100;
			$redutor->reduce($origem, $destino, $dest_width, $dest_height, $max_width, $max_height);	

			//unlink($tempfile);
		}	
	}
?>