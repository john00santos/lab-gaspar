<?
	class redutor {
		static private $instance;
		static public function singleton() {
			if (!isset(self::$instance)) {
				$c = __CLASS__;
				self::$instance = new $c;
			}
	
			return self::$instance;
		}
		public function reduce ($origem, $destino, $dest_width, $dest_height, $max_width, $max_height, $Tamanho = 100, $Qualidade = 90) {
			
			$Ext = strtolower(substr($destino,-4));
			
			if(strtolower($Ext)==".jpg"){
				$ExtFunc = "Jpeg";
			}
			elseif(strtolower($Ext)==".gif"){
				$ExtFunc = "Gif";
			}
			elseif(strtolower($Ext)==".png"){
				$ExtFunc = "Png";
			}
			
			$FUNCTION = "ImageCreateFrom".$ExtFunc;	
			$img = $FUNCTION($origem);
			
			$width = imagesx($img);
			$height = imagesy($img);
			
			$scale = min($max_width/$width, $max_height/$height);
			
			$new_width = floor($scale*$width);
			$new_height = floor($scale*$height);
			$xpos = ($dest_width - $new_width)/2;
			
			$img_nova = imagecreatetruecolor($dest_width, $dest_height);
			$white = imagecolorallocate($img_nova, 0, 0, 0);
			imagefill($img_nova, 0, 0, $white);
			
			

			imagecopyresampled($img_nova, $img, $xpos, 0, 0, 0, $new_width, $new_height, $width, $height);
			
	
			ImageJpeg($img_nova,$destino,$Qualidade);
			ImageDestroy($img_nova);
			ImageDestroy($img);		
	
			return true;
			
		}
	}
	$redutor = redutor::singleton();

?>