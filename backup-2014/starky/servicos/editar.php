<?
	require '../scriptsforload.php';
	require 'classes/servicos.php';	
	$adm_servicos->loadValues($_GET['id']);
	$adm_servicos->getLastValues();
        
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>



<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador #pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#paginas {
		float:none;
		clear:both;
	}
</style>
<script src="../objects/textcounter/js/textcounter.js" type="text/javascript" language="javascript"></script>
<script src="../objects/bitly/js/bitly.js" type="text/javascript" language="javascript"></script>
<script src="../filesmanager/js/basic_functions.js" type="text/javascript" language="javascript"></script>
<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	
<? require 'menu.php'; ?>

<div id="corpo">
<div class="titlearea">Editar Serviço</div>
<form name="form" method="post" action="actions.php?host=<? echo $prop['host']; ?>&act=editar&id=<? echo $_GET['id']; ?>" enctype="multipart/form-data"/>

<input type="hidden" name="MAX_FILE_SIZE" value="21474836480">
<input type="hidden" name="nextid" value="<? echo $_GET['id']; ?>" />




<h4>Título:</h4>
<input type="text" name="fields[titulo]" maxlength="70" class="text_titulo" value="<? echo $adm_servicos->fields['titulo']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<small>Máx: 70 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />

<h4>Gerenciador de Arquivos:</h4>
<input type="button" value="Abrir" onclick="open_filmanager(texto, 'editor')"/><br />
<small>Para anexar arquivos ao post, clique aqui</small>
<br />
<br />

<? require 'richtext_html.php'; ?>
<br />
<br />

<h4>Subtitulo:</h4>
<input type="text" name="fields[subtitulo]" maxlength="120" class="text_titulo" value="<? echo $adm_servicos->fields['subtitulo']; ?>" onchange="textCounter(this, 'subtitulo', 'destcountfour')" onkeyup="textCounter(this, 'subtitulo', 'destcountfour')"/><br />
<small>Máx: 120 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountfour"></div>
<br />

<h4>Descrição:</h4>
<input type="text" name="fields[descricao]" maxlength="160" class="text_titulo" value="<? echo $adm_servicos->fields['descricao']; ?>" onchange="textCounter(this, 'descricao', 'destcounttwo')" onkeyup="textCounter(this, 'descricao', 'destcounttwo')"/><br />
<small>Máx: 160 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcounttwo"></div>
<br />

<h4>Texto para Twitter:</h4>
<input type="text" name="fields[texto_twitter]" id="fields[texto_twitter]" maxlength="99" class="text_titulo" value="<? echo $adm_servicos->fields['texto_twitter']; ?>" onchange="textCounter(this, 'twitter', 'destcountthree')" onkeyup="textCounter(this, 'twitter', 'destcountthree')"/><br />
<small>Máx: 99 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountthree"></div>
<br />

<h4>Tags:</h4>
<input type="text" name="fields[tags]" class="text_titulo" value="<? echo $adm_servicos->fields['tags']; ?>"/><br />
<small>Separe tags por vírgulas</small>
<br />
<br />


<h4>Imagem Pequena:</h4>
<input type="file" name="imagempequena" class="text"/><br />
<small>Resolução: 166 x 221px</small>
<br />
<img src="http://laboratoriogaspar.com.br/site/imagens/servicos/<? echo $_GET['id']; ?>/foto_miniatura.jpg" width="166" height="221" /><br />
<br />
<br />

<h4>Imagem Grande:</h4>
<input type="file" name="imagemgrande" class="text"/><br />
<small>Resolução: 442 x 157px</small><br />
<img src="http://laboratoriogaspar.com.br/site/imagens/servicos/<? echo $_GET['id']; ?>/foto_grande.jpg" width="442" height="157" />
<br />
<br />

<input type="submit" value="Enviar" onclick="saveall()" />

<div id="control"></div>						
</form>

</div>
</body>
</html>