<?
	require '../scriptsforload.php';
	require 'classes/informe.php';	
	$adm_informe->loadValues($_GET['id']);
	$adm_informe->getLastValues();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>



<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador #pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#paginas {
		float:none;
		clear:both;
	}
</style>
<script src="../objects/textcounter/js/textcounter.js" type="text/javascript" language="javascript"></script>
<script src="../objects/bitly/js/bitly.js" type="text/javascript" language="javascript"></script>
<script src="../filesmanager/js/basic_functions.js" type="text/javascript" language="javascript"></script>
<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	
<? require 'menu.php'; ?>

<div id="corpo">
<div class="titlearea">Ver Informe - <a href="editar.php?id=<? echo $_GET['id']; ?>">Editar Post</a></div>
<form name="form" method="post" action="actions.php?host=<? echo $prop['host']; ?>&act=ativar&id=<? echo $_GET['id']; ?>" enctype="multipart/form-data"/>


<h4>Título:</h4>
<input type="text" name="fields[titulo]" maxlength="70" class="text_titulo" value="<? echo $adm_informe->fields['titulo']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<br />

<? require 'richtext_html.php'; ?>
<br />
<br />

<h4>Subtitulo:</h4>
<input type="text" name="fields[subtitulo]" maxlength="120" class="text_titulo" value="<? echo $adm_informe->fields['subtitulo']; ?>" onchange="textCounter(this, 'subtitulo', 'destcountfour')" onkeyup="textCounter(this, 'subtitulo', 'destcountfour')"/><br />
<br />

<h4>Descrição:</h4>
<input type="text" name="fields[descricao]" maxlength="160" class="text_titulo" value="<? echo $adm_informe->fields['descricao']; ?>" onchange="textCounter(this, 'descricao', 'destcounttwo')" onkeyup="textCounter(this, 'descricao', 'destcounttwo')"/><br />
<br />

<h4>Texto para Twitter:</h4>
<input type="text" name="fields[texto_twitter]" id="fields[texto_twitter]" maxlength="99" class="text_titulo" value="<? echo $adm_informe->fields['texto_twitter']; ?>" onchange="textCounter(this, 'twitter', 'destcountthree')" onkeyup="textCounter(this, 'twitter', 'destcountthree')"/><br />
<br />

<h4>Tags:</h4>
<input type="text" name="fields[tags]" class="text_titulo" value="<? echo $adm_informe->fields['tags']; ?>"/><br />
<br />
<br />


<h4>Imagem Pequena:</h4>
<img src="http://laboratoriogaspar.com.br/site/imagens/informe/<? echo $_GET['id']; ?>/foto_miniatura.jpg" /><br />
<br />
<br />

<h4>Imagem Grande:</h4>
<img src="http://laboratoriogaspar.com.br/site/imagens/informe/<? echo $_GET['id']; ?>/foto_grande.jpg" />
<br />
<br />

<input type="submit" value="Publicar" />

<div id="control"></div>						
</form>

</div>
</body>
</html>