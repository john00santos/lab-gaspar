<?
	require_once 'classes/informe.php';

        function tratarImgs($id) {
		$pastaImgRoot = $_SERVER['DOCUMENT_ROOT']."site/imagens/informe/".$id."/";
		@mkdir($pastaImgRoot);
		
                $tempfile = $pastaImgRoot."foto_miniatura.jpg";
                if(!empty($_FILES['imagempequena']['tmp_name'])){
                    unlink($tempfile);
                    move_uploaded_file($_FILES['imagempequena']['tmp_name'], $tempfile);
                }
                
                $tempfile = $pastaImgRoot."foto_grande.jpg";
                if(!empty($_FILES['imagemgrande']['tmp_name'])){
                    unlink($tempfile);                                
                    move_uploaded_file($_FILES['imagemgrande']['tmp_name'], $tempfile);
                }
	}
	
	
	function trataDados() {
		
		require_once '../classes/textActions.php';
		require_once '../classes/dater.php';
		require_once '../classes/permalinks.php';
		
		$fields['titulo'] = $titulo = utf8_decode($_POST['fields']['titulo']);
		$fields['subtitulo'] = $subtitulo = utf8_decode($_POST['fields']['subtitulo']);
		$fields['descricao'] = $descricao = utf8_decode($_POST['fields']['descricao']);
		$fields['texto_twitter'] = $texto_twitter = utf8_decode($_POST['fields']['texto_twitter']);
		
		$fields['data'] = $data = date("Y-m-d H:m:s");
		$fields['data_extenso'] = $data_extenso = $dater->datar(date("Y-n-d"));
		$fields['tags'] = $tags = utf8_decode($_POST['fields']['tags']);
		
		$fields['resumo'] = $resumo = utf8_decode(str_replace("'", "\'", $textActions->strTrimTotal($_POST['ftexts']['1'])));
		$fields['texto'] = $texto = utf8_decode(str_replace("'", "\'", $textActions->strTrimTotal($_POST['ftexts']['2'])));
		
		$fields['permalink'] = $permalink = $permalinks->generate($_POST['fields']['titulo']);
		
		$fields['id'] = $id = $_POST['nextid'];
		
		$bitlyurl = "http://laboratoriogaspar.com.br/starky/objects/bitly/bitly_generator.php?url=".urlencode("http://laboratoriogaspar.com.br/site/informe/".$id."/".$permalink.".html");
		$fields['bitly'] = $bitly = file_get_contents($bitlyurl);
		
		$fields['level'] = $level = $_SESSION['user_level'];
		$fields['author'] = $author = $_SESSION['user_i_id'];
		
		return $fields;
	}
	
		
	switch($_GET['act']) {
		case "criar":			
			
			$fields = trataDados();		
			
			
			$adm_informe->addValues($fields['id'], $fields['titulo'], $fields['subtitulo'], $fields['descricao'], $fields['texto_twitter'], $fields['data'], $fields['data_extenso'], $fields['tags'], $fields['resumo'], $fields['texto'], $fields['permalink'], $fields['bitly'], $fields['level'], $fields['author']);
			tratarImgs($fields['id']);
			
			
			?>
            	<script>
					alert("Post adicionado com sucesso.");
					document.location.href="listar.php";
					
				</script>
            <?
			break;
		case "editar":
					
			$fields = trataDados();	
			$id = $adm_informe->editValues($fields['id'], $fields['titulo'], $fields['subtitulo'], $fields['descricao'], $fields['texto_twitter'], $fields['data'], $fields['data_extenso'], $fields['tags'], $fields['resumo'], $fields['texto'], $fields['permalink'], $fields['bitly'], $fields['level'], $fields['author']);
            
                            if(file_exists($_SERVER['DOCUMENT_ROOT']."site/imagens/informe/".$id."/foto_miniatura.jpg"))
                                unlink($_SERVER['DOCUMENT_ROOT']."site/imagens/informe/".$id."/foto_miniatura.jpg");
                           if(file_exists($_SERVER['DOCUMENT_ROOT']."site/imagens/informe/".$id."/foto_grande.jpg"))
                                unlink($_SERVER['DOCUMENT_ROOT']."site/imagens/informe/".$id."/foto_grande.jpg");
                           
			tratarImgs($fields['id']);
	
			?>
            	<script>
					alert("Post editado com sucesso.");
					document.location.href="listar.php";
				</script>
            <?
			break;
		case "deletar":
			$adm_informe->delete($_GET['id']);
			?>
            	<script>
					alert("Post deletado com sucesso");
					document.location.href="listar.php";
				</script>
            <?
			break;
		case "ativar":
			$adm_informe->ativar($_GET['id']);
			?>
				<script language="javascript">
					var url = "listar.php?cat=<? echo $cat; ?>";
					document.location.href=url;
				</script>
			<? 
	}
	//header("Location: http://".$_GET['host']."/bandaxavecodemenina.com.br/starky/informe/listar.php?ipp=".$_GET['ipp']."&pg=".$_GET['pg']);
?>