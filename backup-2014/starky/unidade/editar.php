<?
	require '../scriptsforload.php';
	require 'classes/unidade.php';
	$adm_unidade->loadValues($_GET['id']);
	$adm_unidade->getLastValues();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>



<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador #pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#paginas {
		float:none;
		clear:both;
	}
</style>
<script src="../objects/textcounter/js/textcounter.js" type="text/javascript" language="javascript"></script>
<script src="../objects/bitly/js/bitly.js" type="text/javascript" language="javascript"></script>
<script src="../filesmanager/js/basic_functions.js" type="text/javascript" language="javascript"></script>
<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	
<? require 'menu.php'; ?>

<div id="corpo">
<div class="titlearea">Criar Unidade de Atendimento</div>
<form name="form" method="post" action="actions.php?host=<? echo $prop['host']; ?>&act=editar&id=<? echo $_GET['id']; ?>" enctype="multipart/form-data"/>

<input type="hidden" name="MAX_FILE_SIZE" value="21474836480">
<input type="hidden" name="nextid" value="<? echo $_GET['id']; ?>" />




<h4>Nome da Unidade:</h4>
<input type="text" name="fields[nome]" maxlength="70" class="text_titulo" value="<?=$adm_unidade->fields['nome']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<small>Máx: 200 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />

<h4>Mapa (insira o código fornecido pelo Google):</h4>
<textarea cols="50" rows="5" name="fields[mapa]" value="<?=$adm_unidade->fields['mapa']; ?>"></textarea>
&nbsp;&nbsp;&nbsp;<div id="destcounttwo"></div>
<br />

<h4>Endereço:</h4>
<input type="text" name="fields[endereco]" id="fields[endereco]" maxlength="99" class="text_titulo" value="<?=$adm_unidade->fields['endereco']; ?>" /><br />
&nbsp;&nbsp;&nbsp;<div id="destcountthree"></div>
<br />

<h4>Como chegar:</h4>
<? require 'richtext_html.php'; ?>
&nbsp;&nbsp;&nbsp;<div id="destcountfour"></div>
<br />

<input type="submit" value="Enviar" onclick="saveall()" />

<div id="control"></div>
</form>

</div>
</body>
</html>