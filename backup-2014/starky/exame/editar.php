<?
	require '../scriptsforload.php';
	require 'classes/exame.php';
	$adm_exame->loadValues($_GET['id']);
	$adm_exame->getLastValues();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laboratório Gaspar - Intranet</title>
<link rel="stylesheet" type="text/css" href="../css/defaults.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" type="text/css" href="css/statistics.css" />
</head>
<body>



<style>
	body, * {
		font-family: Tahoma, Geneva, sans-serif;
		font-size:12px;
	}
	input.text {
		width: 300px;
	}
	#paginador {
		font-size:10px;
		font-family:"Trebuchet MS";
		color:#0000000;
		width:100%;
		float:none;
		clear:both;
	}
	#paginador #pg {
		background-image:url(images/icones/pagename_1.gif);
		width:100px;
		height:15px;
		padding-top:5px;
		text-align:center;
		float:left;
	}
	#paginador a {
		color:#000000;
	}
	#paginador a:hover {
		color:#FF0000;
	}
	#paginas {
		float:none;
		clear:both;
	}
</style>
<script src="../objects/textcounter/js/textcounter.js" type="text/javascript" language="javascript"></script>
<script src="../objects/bitly/js/bitly.js" type="text/javascript" language="javascript"></script>
<script src="../filesmanager/js/basic_functions.js" type="text/javascript" language="javascript"></script>
<div id="container">
<? require '../login.php'; ?>
<? require '../modulesi.php'; ?>	
<? require 'menu.php'; ?>

<div id="corpo">
<div class="titlearea">Editar Orientação para exame</div>
<form name="form" method="post" action="actions.php?host=<? echo $prop['host']; ?>&act=editar&id=<? echo $_GET['id']; ?>" enctype="multipart/form-data"/>

<input type="hidden" name="MAX_FILE_SIZE" value="21474836480">
<input type="hidden" name="nextid" value="<? echo $_GET['id']; ?>" />




<h4>Nome:</h4>
<input type="text" name="fields[nome]" maxlength="70" class="text_titulo" value="<? echo $adm_exame->fields['nome']; ?>" onchange="textCounter(this, 'title', 'destcountone')" onkeyup="textCounter(this, 'title', 'destcountone')"/><br />
<small>Máx: 200 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountone"></div>
<br />


<h4>Informações Gerais:</h4>
<? require 'richtext_html.php'; ?>
<br />
<br />

<h4>Agendamento:</h4>
<input type="text" name="fields[agenda]" maxlength="200" class="text_titulo" value="<? echo $adm_exame->fields['agenda']; ?>" onchange="textCounter(this, 'subtitulo', 'destcountfour')" onkeyup="textCounter(this, 'subtitulo', 'destcountfour')"/><br />
<small>Máx: 200 caracteres</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountfour"></div>
<br />

<h4>Preço Particular:</h4>
R$ <input type="text" name="fields[preco]" maxlength="160" class="text_titulo" value="<? echo $adm_exame->fields['preco']; ?>" onchange="textCounter(this, 'descricao', 'destcounttwo')" onkeyup="textCounter(this, 'descricao', 'destcounttwo')"/><br />
&nbsp;&nbsp;&nbsp;<div id="destcounttwo"></div>
<br />

<h4>Preço Fleury:</h4>
R$ <input type="text" name="fields[preco2]" maxlength="160" class="text_titulo" value="<? echo $adm_exame->fields['preco2']; ?>" onchange="textCounter(this, 'descricao', 'destcounttwo')" onkeyup="textCounter(this, 'descricao', 'destcounttwo')"/><br />
&nbsp;&nbsp;&nbsp;<div id="destcounttwo"></div>
<br />

<h4>Preço Laboratório Hermes Pardinni:</h4>
R$ <input type="text" name="fields[preco3]" maxlength="160" class="text_titulo" value="<? echo $adm_exame->fields['preco3']; ?>" onchange="textCounter(this, 'descricao', 'destcounttwo')" onkeyup="textCounter(this, 'descricao', 'destcounttwo')"/><br />
&nbsp;&nbsp;&nbsp;<div id="destcounttwo"></div>
<br />

<h4>Prazo para Entrega:</h4>
<input type="text" name="fields[prazo]" id="fields[prazo]" maxlength="99" class="text_titulo" value="<? echo $adm_exame->fields['prazo']; ?>" onchange="textCounter(this, 'twitter', 'destcountthree')" onkeyup="textCounter(this, 'twitter', 'destcountthree')"/><br />
&nbsp;&nbsp;&nbsp;<div id="destcountthree"></div>
<br />

<h4>Convênios:</h4>
<input type="text" name="fields[convenio]" class="text_titulo" value="<? echo $adm_exame->fields['convenio']; ?>" /><br />
<small>Separe convenios usando " ; "</small><br />
&nbsp;&nbsp;&nbsp;<div id="destcountfive"></div>
<br />

<input type="submit" value="Enviar" onclick="saveall()" />

<div id="control"></div>						
</form>

</div>
</body>
</html>