<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TESTE</title>
    <link rel="stylesheet" href="">
    <script src="jquery/1.10.2/jquery-1.10.2.min.js"></script>
</head>
<body>

<style type="text/css" media="screen">

.item_convenio{
    cursor: pointer;
}

.itens_convenio{
    display: inline-block;
    line-height: 28px;
}

#menu_itens{
    text-align: center;
}
    
</style>

<?php
    $cliente = new SoapClient('http://201.38.233.30:8200/MatrixNET/wsrvPortalNet.svc?wsdl');
    
    $funcao1 = 'ListaCoberturaWeb';

    $funcao2 = 'ListaConveniosWeb';
     
    $opcoes = array('location' => 'http://201.38.233.30:8200/MatrixNET/wsrvPortalNet.svc');

    $resultado =  $cliente->__soapCall($funcao2, $opcoes);

    $geral = json_decode(json_encode($resultado->conveniosWeb->ConvenioWeb), true);

    $tamanhoArray = sizeof($resultado->conveniosWeb->ConvenioWeb);

    $planos = array();

    echo("<nav id='menu_itens'>");

    for($i=0; $i<$tamanhoArray; $i++){
        $item = $geral[$i]['Descricao'];
        $itemCodigo = $geral[$i]['Codigo'];

        echo "<li class='itens_convenio'><a class='item_convenio' onclick='exibePagina(\"$itemCodigo\",\"$item\");'  title='$item'>$item</a></li><span> &nbsp;|&nbsp; </span>";
    } 

    echo("</nav>");

?>

<div id="procedimentos">
    <h1>Procedimentos</h1>
    <p></p>
</div>
    
</body>

<script type="text/javascript">
    
function exibePagina(convenio,convenioNome){

    $('#procedimentos p').html('Carregando...');

    $.ajax({
        url: 'lista-cobertura.php',
        type: 'GET',
        data: {convenio: convenio, convenioNome: convenioNome},
        beforeSend: function(){
            console.log('carregando...')
        }
    })
    .done(function(data) {
        $('#procedimentos p').html(data);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });

}  

</script>


</html>