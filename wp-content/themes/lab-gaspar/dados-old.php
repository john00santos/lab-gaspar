<?php
/**
 * Template Name: Dados
 * The main template file for display page.
 *
 * @package WordPress
*/

include (get_template_directory() . "/lib/fallback.home.lib.php");

/**
*	Get Current page object
**/
$page = get_page($post->ID);

/**
*	Get current page id
**/
if(!isset($current_page_id) && isset($page->ID))
{
    $current_page_id = $page->ID;
}
else
{
	global $query_string;
	query_posts($query_string . "&page_id=".$current_page_id);
}

if(!isset($hide_header) OR !$hide_header)
{
	get_header(); 
}

if(!isset($hide_header) OR !$hide_header)
{
?>	
</div>

<div class="page_caption">
	<div class="caption_inner">
		<div class="caption_header">
			<h1 class="cufon"><span><?php the_title(); ?></span></h1>
			<?php
			$page_description = get_post_meta($current_page_id, 'page_description', true);
			
			if(!empty($page_description))
			{
			?>
				<span class="page_description"><?php echo $page_description; ?></span>
			<?php
			}
			?>
		</div>
	</div>
	<br class="clear"/>
</div>
<br class="clear"/>

<!-- Begin content -->
<div id="content_wrapper">

    <div class="inner">
    
    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    	
    		<div class="standard_wrapper">
    	
<?php
}
?>		
    		<?php
    		$client = new SoapClient('http://labmobile.labgaspar.com.br/MatrixNET/wsrvPortalNet.svc?wsdl');
    		 
    		// $function = 'ListaConveniosWeb';
    		$function_two = 'ListaProcedimentos';
    		 
    		// $arguments= array('ConvertTemp' => array(
    		//                         'Temperature'   => 31,
    		//                         'FromUnit'      => 'degreeCelsius',
    		//                         'ToUnit'        => 'degreeFahrenheit'
    		//                 ));
    		$options = array('location' => 'http://labmobile.labgaspar.com.br/MatrixNET/wsrvPortalNet.svc');
    		 
    		// $result = $client->__soapCall($function, $options);

    		$result_two = $client->__soapCall($function_two, $options);

    		$procedimentos = $result_two->procedimentosWeb->ProcedimentoWeb;

    		$descricao = $result->conveniosWeb->ConvenioWeb;


    		// Lista Procedimentos

    		for ($i=0; $i <= sizeof($procedimentos) ; $i++) { 
    			echo ($procedimentos[$i]->Nome) . '<br/>' ;
    		}
    		// Lista Convênios

    		// for($i=0; $i<= sizeof($descricao); $i++){
    		// 	echo ($descricao[$i]->Descricao) . '<br/>';
    		// }

    		?>
    
    		</div>
    		<br class="clear"/>
    		
   		 </div>
   	 	<!-- End main content -->
   	 	
	</div>
</div>
			
<?php 
if(!isset($hide_header) OR !$hide_header OR is_null($hide_header))
{
?>			
</div>
<?php get_footer(); ?>

<?php
}
?>