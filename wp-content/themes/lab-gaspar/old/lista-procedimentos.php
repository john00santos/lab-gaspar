<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TESTE</title>
    <link rel="stylesheet" href="">
    <script src="jquery/1.10.2/jquery-1.10.2.min.js"></script>
    <script src="highlight.js"></script>
</head>
<body>

<style type="text/css" media="screen">

#bg_instrucao{
    width: 100%;
    height: 100%;
    position: fixed;
    background: rgba(0,0,0,0.8);
    top: 0px;
    left: 0px;
    display: none;
}

#instrucao{
    width: 900px;
    height: 600px;
    position: fixed;
    top: 50%;
    left: 50%;
    margin-top: -300px;
    margin-left: -450px;
    background: white;
    padding: 25px;
    display: none;
}

#fechar{
    width: 30px;
    height: 30px;
    border-radius: 100%;
    border: 2px solid #bbb;
    cursor: pointer;
    float: right;
    text-align: center;
    font-size: 18px;
    line-height: 32px;
}

#fechar:hover{
    background: #bbb;
}

pre{
    width: 100%;
    word-wrap: break-word;
}
    
</style>

<?php
    $cliente = new SoapClient('http://201.38.233.30/MatrixNET/wsrvPortalNet.svc?wsdl');
    
    $funcao1 = 'ListaProcedimentos';

    $argumentos = array('ListaProcedimentos' => array(

        'nomeProcedimento' => ''
    ));

    $opcoes = array('location' => 'http://201.38.233.30/MatrixNET/wsrvPortalNet.svc');

    $resultado =  $cliente->__soapCall($funcao1, $argumentos, $opcoes);

    $geral = json_decode(json_encode($resultado->procedimentosWeb->ProcedimentoWeb), true);

?>

<input type="text" id="filter" placeholder="DIGITE O NOME DO PROCEDIMENTO" style="width:400px; height: 26px; background: url(icone-filtro.png) no-repeat right; text-transform: uppercase;"><br/><br/>

<?php

    for ($i=0; $i < sizeof($geral) ; $i++) {

        $codigo =  $geral[$i]["Codigo"];
        
        echo "<div class='jcorgFilterTextParent'><a style='cursor:pointer;' class='jcorgFilterTextChild' onclick='exibeInstrucoes(\"$codigo\");' >" . $geral[$i]["Nome"] . "</a></div>";
    }

?>

<div id="bg_instrucao"></div>

<div id="instrucao">
    <div id="fechar">X</div>
    <h4>Instruções</h4>
    <p></p>    
</div>



  
</body>


</html>

<script type="text/javascript">

    function exibeInstrucoes(codigo){

        jQuery.ajax({
            url: 'http://laboratoriogaspar.com.br/wp-content/themes/lab-gaspar/old/lista-instrucoes.php',
            type: 'GET',
            data: {codigo: codigo},
            beforeSend: function(){
                jQuery('#instrucao p').html('Carregando...');
                jQuery('#bg_instrucao').fadeIn('fast',function(){
                    jQuery('#instrucao').fadeIn('fast');
                })
            }
        })
        .done(function(data) {
            if (data == ""){
                jQuery('#instrucao p').html('Não disponível.');
            }else{
                jQuery('#instrucao p').html(data);
            }
        })
        .fail(function() {
            console.log("error");
        })

    }

    jQuery('#fechar').click(function(event) {
        jQuery('#instrucao').fadeOut('fast',function(){
            jQuery('#bg_instrucao').fadeOut('fast');
        })
    });


    

    jQuery(document).ready(function(){


        jQuery("#filter").jcOnPageFilter({animateHideNShow: true,
                focusOnLoad:true,
                highlightColor:'yellow',
                textColorForHighlights:'#000000',
                caseSensitive:false,
                hideNegatives:true,
                parentLookupClass:'jcorgFilterTextParent',
                childBlockClass:'jcorgFilterTextChild'
        });
        
    });        



</script>