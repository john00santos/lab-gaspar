<?php
/**
 * The main template file for display page.
 * Template Name: Dados
 *
 * @package WordPress
*/

include (get_template_directory() . "/lib/fallback.home.lib.php");

/**
*	Get Current page object
**/
$page = get_page($post->ID);

/**
*	Get current page id
**/
if(!isset($current_page_id) && isset($page->ID))
{
    $current_page_id = $page->ID;
}
else
{
	global $query_string;
	query_posts($query_string . "&page_id=".$current_page_id);
}

if(!isset($hide_header) OR !$hide_header)
{
	get_header(); 
}

if(!isset($hide_header) OR !$hide_header)
{

// Check if use page builder
$ppb_form_data_order = '';
$ppb_form_item_arr = array();
$ppb_enable = get_post_meta($current_page_id, 'ppb_enable', true);
?>

<script src=<?php echo "http://www.laboratoriogaspar.com.br/wp-content/themes/lab-gaspar/old/highlight.js"; ?>></script>

<style type="text/css" media="screen">

.item_convenio{
    cursor: pointer;
}

.itens_convenio{
    line-height: 16px;
}

#menu_itens{
    text-align: left;
    list-style: none;
}

#filter{

    background: url(http://www.laboratoriogaspar.com.br/wp-content/themes/lab-gaspar/old/icone-filtro.png) no-repeat 390px center;
    width:400px; 
    height: 26px; 
    text-transform: uppercase !important;
}

.jcorgFilterTextParent{
    line-height: 16px;
    list-style: none;
}
    
</style>


</div>

<div class="page_caption">
	<div class="caption_inner">
		<div class="caption_header">
			<h1 class="cufon"><span><?php the_title(); ?></span></h1>
			<?php
			$page_description = get_post_meta($current_page_id, 'page_description', true);
			
			if(!empty($page_description))
			{
			?>
				<span class="page_description"><?php echo $page_description; ?></span>
			<?php
			}
			?>
		</div>
	</div>
	<br class="clear"/>
</div>
<br class="clear"/>

<!-- Begin content -->
<div id="content_wrapper">

    <div class="inner">
    
    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    	
    		<div class="standard_wrapper">
    	
<?php
}
?>		
    			<?php if ( empty($ppb_enable) && have_posts() ) {
    				while ( have_posts() ) : the_post(); ?>		
    	
    				<?php the_content(); break;  ?>

    			<?php endwhile; 
	    			}
	    			else //Display Page Builder Content
	    			{
	    				pp_apply_builder($current_page_id);
    				}
    			?>

    			<?php
    			    $cliente = new SoapClient('http://201.38.233.30/MatrixNET/wsrvPortalNet.svc?wsdl');
    			    
    			    $funcao1 = 'ListaCoberturaWeb';

    			    $funcao2 = 'ListaConveniosWeb';
    			     
    			    $opcoes = array('location' => 'http://201.38.233.30/MatrixNET/wsrvPortalNet.svc');

    			    $resultado =  $cliente->__soapCall($funcao2, $opcoes);

    			    $geral = json_decode(json_encode($resultado->conveniosWeb->ConvenioWeb), true);

    			    $tamanhoArray = sizeof($resultado->conveniosWeb->ConvenioWeb);

    			    $planos = array();

    			    echo "<h5>Selecione o convênio:</h5><br/>";

    			    echo("<nav id='menu_itens'>");

    			    for($i=0; $i<$tamanhoArray; $i++){
    			        $item = $geral[$i]['Descricao'];
    			        $itemCodigo = $geral[$i]['Codigo'];

    			        echo "<li class='itens_convenio'><a class='item_convenio' onclick='exibePagina(\"$itemCodigo\",\"$item\");'  title='$item'>$item</a></li>";
    			    } 

    			    echo("</nav>");
    			    echo "<br/>";
    			    echo "<br/>";

    			?>

    			<div id="procedimentos">
    			    <p></p>
    			</div>
    
    		</div>
    		<br class="clear"/>
    		
   		 </div>
   	 	<!-- End main content -->
   	 	
	</div>
</div>

<script type="text/javascript">

   
function exibePagina(convenio,convenioNome){

    $j('#procedimentos p').html('Carregando...');

    $j.ajax({
        url: 'http://laboratoriogaspar.com.br/wp-content/themes/lab-gaspar/old/lista-cobertura.php',
        type: 'GET',
        data: {convenio: convenio, convenioNome: convenioNome},
        beforeSend: function(){
            $j('body').animate({scrollTop:1050}, '500');            
        }
    })
    .done(function(data) {
        $j('#procedimentos p').html(data);

        $j("#filter").jcOnPageFilter({animateHideNShow: true,
                focusOnLoad:true,
                highlightColor:'yellow',
                textColorForHighlights:'#000000',
                caseSensitive:false,
                hideNegatives:true,
                parentLookupClass:'jcorgFilterTextParent',
                childBlockClass:'jcorgFilterTextChild'
        });
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });

}  

</script>
			
<?php 
if(!isset($hide_header) OR !$hide_header OR is_null($hide_header))
{
?>			
</div>
<?php get_footer(); ?>

<?php
}
?>