<?php
/**
 * The main template file for display page.
 * Template Name: Procedimentos
 *
 * @package WordPress
*/

include (get_template_directory() . "/lib/fallback.home.lib.php");

/**
*	Get Current page object
**/
$page = get_page($post->ID);

/**
*	Get current page id
**/
if(!isset($current_page_id) && isset($page->ID))
{
    $current_page_id = $page->ID;
}
else
{
	global $query_string;
	query_posts($query_string . "&page_id=".$current_page_id);
}

if(!isset($hide_header) OR !$hide_header)
{
	get_header(); 
}

if(!isset($hide_header) OR !$hide_header)
{

// Check if use page builder
$ppb_form_data_order = '';
$ppb_form_item_arr = array();
$ppb_enable = get_post_meta($current_page_id, 'ppb_enable', true);
?>

<style type="text/css" media="screen">

#bg_instrucao{
    width: 100%;
    height: 100%;
    position: fixed;
    background: rgba(0,0,0,0.8);
    top: 0px;
    left: 0px;
    display: none;
}

#instrucao{
    width: 900px;
    height: 700px;
    position: fixed;
    top: 50%;
    left: 50%;
    margin-top: -350px;
    margin-left: -450px;
    background: white;
    padding: 25px;
    display: none;
}

#fechar{
    width: 30px;
    height: 30px;
    border-radius: 100%;
    border: 2px solid #bbb;
    cursor: pointer;
    float: right;
    text-align: center;
    font-size: 18px;
    line-height: 32px;
}

#fechar:hover{
    background: #bbb;
}

#filter{

    background: url(http://www.laboratoriogaspar.com.br/wp-content/themes/lab-gaspar/old/icone-filtro.png) no-repeat 390px center;
    width:400px; 
    height: 26px; 
    text-transform: uppercase !important;

}

pre{
    width: 96% !important;
    margin-left: -15px;
    word-wrap: break-word;
    background: none !important;
}
    
</style>


</div>

<div class="page_caption">
	<div class="caption_inner">
		<div class="caption_header">
			<h1 class="cufon"><span><?php the_title(); ?></span></h1>
			<?php
			$page_description = get_post_meta($current_page_id, 'page_description', true);
			
			if(!empty($page_description))
			{
			?>
				<span class="page_description"><?php echo $page_description; ?></span>
			<?php
			}
			?>
		</div>
	</div>
	<br class="clear"/>
</div>
<br class="clear"/>

<!-- Begin content -->
<div id="content_wrapper">

    <div class="inner">
    
    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    	
    		<div class="standard_wrapper">
    	
<?php
}
?>		
    			


                <!-- CONTENT -->

    
    		</div>
    		<br class="clear"/>
    		
   		 </div>
   	 	<!-- End main content -->
   	 	
	</div>
</div>

<script src=<?php echo "http://www.laboratoriogaspar.com.br/wp-content/themes/lab-gaspar/old/highlight.js"; ?>></script>

<script type="text/javascript">



$j.ajax({
    url: 'http://labgaspar.localhost/wp-content/themes/lab-gaspar/old/get-procedimentos.php',
	crossDomain: true,
    beforeSend: function(){
        $j('.standard_wrapper').html('<p id="hide_carregar">Carregando...</p>');
    }
})
.done(function(data) {

    $j('#hide_carregar').hide();
    
    $j('.standard_wrapper').append(data);

    $j('#fechar').click(function(event) {
        $j('#instrucao').fadeOut('fast',function(){
            $j('#bg_instrucao').fadeOut('fast');
        })
    });

    $j("#filter").jcOnPageFilter({animateHideNShow: true,
            focusOnLoad:true,
            highlightColor:'yellow',
            textColorForHighlights:'#000000',
            caseSensitive:false,
            hideNegatives:true,
            parentLookupClass:'jcorgFilterTextParent',
            childBlockClass:'jcorgFilterTextChild'
    });

})
.fail(function() {
    console.log("error");
})
function jsonpCallbackRest(data){
	return false;
}


</script>
			
<?php 
if(!isset($hide_header) OR !$hide_header OR is_null($hide_header))
{
?>			
</div>
<?php get_footer(); ?>

<?php
}
?>