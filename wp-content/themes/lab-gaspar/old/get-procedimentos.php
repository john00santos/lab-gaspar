<?php
    $cliente = new SoapClient('http://201.38.233.30/MatrixNET/wsrvPortalNet.svc?wsdl');
    //var_dump($cliente); die();
    $funcao1 = 'ListaProcedimentos';

    $argumentos = array('ListaProcedimentos' => array(

        'nomeProcedimento' => ''
    ));

    $opcoes = array('location' => 'http://201.38.233.30/MatrixNET/wsrvPortalNet.svc');

    $resultado =  $cliente->__soapCall($funcao1, $argumentos, $opcoes);

    $geral = json_decode(json_encode($resultado->procedimentosWeb->ProcedimentoWeb), true);

?>

<p>
    Clique sobre o nome do procedimento para visualizar as intruções de preparo.<br/>
    Utilize o campo abaixo para filtrar os resultados.<br/><br/>
</p>

<input type="text" id="filter" placeholder="DIGITE O NOME DO PROCEDIMENTO"><br/><br/><br/><br/>

<?php

    for ($i=0; $i < sizeof($geral) ; $i++) {

        $codigo =  $geral[$i]["Codigo"];
        
        echo "<div class='jcorgFilterTextParent'><a style='cursor:pointer;' class='jcorgFilterTextChild' onclick='exibeInstrucoes(\"$codigo\");' >" . $geral[$i]["Nome"] . "</a></div>";
    }

?>


<div id="bg_instrucao"></div>

<div id="instrucao">
    <div id="fechar">X</div>
    <h4>Instruções</h4>
    <p></p>    
</div>


<script type="text/javascript">
    

    function exibeInstrucoes(codigo){

        $j.ajax({
            url: 'http://labgaspar.localhost/wp-content/themes/lab-gaspar/old/lista-instrucoes.php',
            type: 'GET',
            data: {codigo: codigo},
            beforeSend: function(){
                $j('#instrucao p').html('Carregando...');
                $j('#bg_instrucao').fadeIn('fast',function(){
                    $j('#instrucao').fadeIn('fast');
                })
            }
        })

        .done(function(data) {

            if (data == ""){
                console.log('sucesso');
                console.log(data);
                $j('#instrucao p').html('Não disponível.');
            }else{
                $j('#instrucao p').html(data);
            }
        })

        .fail(function() {
            console.log("error");
        })

    }     


</script>