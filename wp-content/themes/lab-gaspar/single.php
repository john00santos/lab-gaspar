<?php
/**
 * The main template file for display single post page.
 *
 * @package WordPress
*/

//If portfolio or gallery content then go to another template
if($post->post_type == 'portfolios')
{
	include (TEMPLATEPATH . "/portfolio_single.php");
    exit;
}

if($post->post_type == 'gallery')
{
	include (TEMPLATEPATH . "/gallery.php");
    exit;
}

if($post->post_type == 'events')
{
	include (TEMPLATEPATH . "/event_single.php");
    exit;
}

if($post->post_type == 'sermons')
{
	include (TEMPLATEPATH . "/sermon_single.php");
    exit;
}

get_header(); 

$page = get_page($post->ID);
$current_page_id = $page->ID;
$page_sidebar = 'Blog Sidebar';

?>
		
	</div>
	
	<div class="page_caption">
    	<div class="caption_inner">
    		<div class="caption_header">
    			<h1 class="cufon"><span><?php echo _e( the_title(), THEMEDOMAIN ); ?></span></h1>
    		</div>
    	</div>
    	<br class="clear"/>
    </div>
    <br class="clear"/>
	
	<!-- Begin content -->
	<div id="content_wrapper">
	    
	    <div class="inner">
	    
	    	<!-- Begin main content -->
	    	<div class="inner_wrapper" style="overflow:hidden;">
	    	
	    		<div class="standard_wrapper">
	    		
	    			<?php
					if (have_posts()) : while (have_posts()) : the_post();
					?>
	    		
	    			<div class="sidebar_content">
						
						<?php
							$image_thumb = '';
							$pp_blog_single_img = get_option('pp_blog_single_img');
														
							if(!empty($pp_blog_single_img) && has_post_thumbnail(get_the_ID(), 'blog'))
							{
							    $image_id = get_post_thumbnail_id(get_the_ID());
							    // $image_thumb = wp_get_attachment_image_src($image_id, 'blog', true);
							    $image_thumb = wp_get_attachment_image_src($image_id, 'blog_f', true);
							}
						?>
						
						<!-- Begin each blog post -->
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> class="post_wrapper"  style="width:960px; overflow:hidden;">
						
							<?php
						        if(isset($image_thumb[0]))
						        {
						    ?>	
						    	<div class="post_img_wrapper" style="width:960px !important; margin-bottom:40px;">
							    	<img style="width:960px; max-width:960px; height:300px; min-height: 300px;>" src="<?php echo $image_thumb[0]; ?>" alt=""/>
							    </div>
						    
						    <?php
						    	}
						    ?>
						    
						    <br class="clear"/>
						    
						    
						    <div class="post_excerpt" style="width:960px; max-width:960px; overflow: hidden;">
						    <?php
						    	the_content();
						    	
						    	$pp_blog_share = get_option('pp_blog_share');
						    	
						    	if(!empty($pp_blog_share))
						    	{
						    ?>	
						    
						    	<br class="clear"/><br/>
						    
							    
							    
							<?php
								}
							?>
							    
						    </div>
						    
						    <br class="clear"/>
						
						</div>
						
						<!-- End each blog post -->
						
						
						
						<?php endwhile; endif; ?>

					</div>
					
					<div class="sidebar_wrapper">
						<div class="sidebar_top" style="border:0"></div>
					
						<div class="sidebar">
							
							<div class="content">
							
								<ul class="sidebar_widget">
									<?php dynamic_sidebar($page_sidebar); ?>
								</ul>
								
							</div>
						
						</div>
						
						<div class="sidebar_bottom"></div>
						<br class="clear"/>
					
					</div>
					
				</div>
				<!-- End main content -->
				
				<br class="clear"/>
			</div>
		</div>
				

<?php get_footer(); ?>