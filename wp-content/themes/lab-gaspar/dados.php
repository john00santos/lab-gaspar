<?php
/**
 * Template Name: Dados
 * The main template file for display page.
 *
 * @package WordPress
*/

include (get_template_directory() . "/lib/fallback.home.lib.php");

/**
*	Get Current page object
**/
$page = get_page($post->ID);

/**
*	Get current page id
**/
if(!isset($current_page_id) && isset($page->ID))
{
    $current_page_id = $page->ID;
}
else
{
	global $query_string;
	query_posts($query_string . "&page_id=".$current_page_id);
}

if(!isset($hide_header) OR !$hide_header)
{
	get_header(); 
}

if(!isset($hide_header) OR !$hide_header)
{
?>


<style type="text/css" media="screen">

.item_convenio{
    cursor: pointer;
}

.itens_convenio{
    display: inline-block;
    line-height: 28px;
}

#menu_itens{
    text-align: center;
}
    
</style>


</div>

<div class="page_caption">
	<div class="caption_inner">
		<div class="caption_header">
			<h1 class="cufon"><span><?php the_title(); ?></span></h1>
			<?php
			$page_description = get_post_meta($current_page_id, 'page_description', true);
			
			if(!empty($page_description))
			{
			?>
				<span class="page_description"><?php echo $page_description; ?></span>
			<?php
			}
			?>
		</div>
	</div>
	<br class="clear"/>
</div>
<br class="clear"/>

<!-- Begin content -->
<div id="content_wrapper">

    <div class="inner">
    
    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    	
    		<div class="standard_wrapper">
    	



                <?php
                    $cliente = new SoapClient('http://201.38.233.30:8200/MatrixNET/wsrvPortalNet.svc?wsdl');
                    
                    $funcao1 = 'ListaCoberturaWeb';

                    $funcao2 = 'ListaConveniosWeb';
                     
                    $opcoes = array('location' => 'http://201.38.233.30:8200/MatrixNET/wsrvPortalNet.svc');

                    $resultado =  $cliente->__soapCall($funcao2, $opcoes);

                    $geral = json_decode(json_encode($resultado->conveniosWeb->ConvenioWeb), true);

                    $tamanhoArray = sizeof($resultado->conveniosWeb->ConvenioWeb);

                    $planos = array();

                    echo("<nav id='menu_itens'>");

                    for($i=0; $i<$tamanhoArray; $i++){
                        $item = $geral[$i]['Descricao'];
                        $itemCodigo = $geral[$i]['Codigo'];

                        echo "<li class='itens_convenio'><a class='item_convenio' onclick='exibePagina(\"$itemCodigo\",\"$item\");'  title='$item'>$item</a></li><span> &nbsp;|&nbsp; </span>";
                    } 

                    echo("</nav>");

                ?>

                <div id="procedimentos">
                    <h1>Procedimentos</h1>
                    <p></p>
                </div>
    
    		</div>
    		<br class="clear"/>
    		
   		 </div>
   	 	<!-- End main content -->
   	 	
	</div>
</div>
			
<?php 
if(!isset($hide_header) OR !$hide_header OR is_null($hide_header))
{
?>			
</div>
<?php get_footer(); ?>

<?php
}
?>

<script type="text/javascript">
    
function exibePagina(convenio,convenioNome){

    $('#procedimentos p').html('Carregando...');

    $.ajax({
        url: 'lista-cobertura.php',
        type: 'GET',
        data: {convenio: convenio, convenioNome: convenioNome},
        beforeSend: function(){
            console.log('carregando...')
        }
    })
    .done(function(data) {
        $('#procedimentos p').html(data);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });

}  

</script>